<?php

namespace App\Http\Controllers;

use App\Models\Department;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Repositories\Traits\Fetcher;

class DepartmentController extends Controller
{
    use Fetcher;

    public function __construct(Department $model)
    {
        $this->model = $model;
    }

    public function index(){
        $department = $this->model->orderBy('name','ASC')->first();

        $department->description = str_replace("&nbsp;"," ",$department->description);

        $departments = $this->getAll();

        return view('pages.departments.index', compact('departments','department'));
    }

    public function single($slug){
        $department = $this->model->with('physicians')->where('slug',$slug)->first();

        $department->description = str_replace("&nbsp;"," ",$department->description);
        return view('pages.departments.index', compact('departments','department'));
    }
}
