<?php

namespace App\Http\Controllers;

use App\Services\EventFetcher;
use App\Models\Event;
use Illuminate\Http\Request;

use App\Http\Requests;

class EventController extends Controller
{
    public function __construct(Event $event,EventFetcher $eventFetcher)
    {
        $this->event = $event;
        $this->eventFetcher = $eventFetcher;
    }

    public function index(){
        $articles = $this->eventFetcher->upcoming('event',6);
        return view('pages.events.index',compact('articles'));
    }

    public function past(){
        $articles = $this->eventFetcher->past('event',6);
        return view('pages.events.index',compact('articles'));
    }

    public function show($slug){
        $article = $this->event->where('slug',$slug)->first();

        return view('pages.media.single-event',compact('article'));
    }
}
