<?php

namespace App\Http\Controllers\Admin;

use App\Repositories\AppointmentRequestManager;
use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\AppointmentRequest;
use Maatwebsite\Excel\Facades\Excel;

class AppointmentController extends Controller
{

    public function __construct(AppointmentRequestManager $manager)
    {
        $this->manager = $manager;
    }

    public function index(){
        return view('admin.appointments.index');
    }

    public function create(){
        return view('admin.appointments.create');
    }

    public function store(Request $request){
        $input = $request->except('_token');

        $this->manager->store($input);

        return view('pages.appointments.success');
    }

    public function view($id){
        $data = AppointmentRequest::with('info')->where('id',$id)->first();

        return view('admin.appointments.view', compact('data'));
    }



    public function report(){

        if(isset($_GET['from']) && isset($_GET['to']))
            $appointments = AppointmentRequest::with('info')->where('created_at', '>=', Carbon::parse($_GET['from'])->format('Y-m-d'))->where('created_at', '<=', Carbon::parse($_GET['to'])->format('Y-m-d'))->get();
        else
            $appointments = AppointmentRequest::with('info')->get();

        $data = [];

        foreach ($appointments as $index=>$appointment){
            $data[$index][] = $appointment->name;
            foreach ($appointment->info as $info){
                if($info->key == 'Primary phone')
                    $data[$index][] = $info->value;
                if($info->key == 'Email')
                    $data[$index][] = $info->value;
            }
            $data[$index][] = $appointment->created_at->format('m-d-Y');
        }

        $d['data'] = $data;

        if(count($data)){
            Excel::create('HP_Appointments_AR_'.$d['data'][0][3], function($excel) use($d) {

                $excel->sheet('HP_Appointments_AR_'.$d['data'][0][3], function($sheet) use($d) {

                    $sheet->loadView('admin.appointments.report',$d);

                });

            })->export('xls');
        } else {
            echo 'None';
        }
//        return view('admin.appointments.report',compact('appointments'));
    }

    public function delete($id){
        AppointmentRequest::where('id',$id)->delete();


        return redirect()->back();
    }
}
