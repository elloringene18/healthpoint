<?php

namespace App\Http\Controllers\Admin;

use App\Models\Article;
use App\Models\ArticleCategory;
use App\Repositories\ArticleManager;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;

class ArticleController extends Controller
{

    public function __construct(Article $article, ArticleManager $articleManager)
    {
        $this->model = $article;
        $this->articles = $articleManager;
    }

    public function index($category){
        $articles = $this->model->where('category_id',$category)->orderBy('created_at','DESC')->get();
        $category = ArticleCategory::where('id',$category)->first();

        return view('admin.articles.index',compact('articles','category'));
    }

    public function create(){
        return view('admin.articles.create');
    }

    public function edit($article_id){
        $article = $this->model->where('id',$article_id)->first();
        return view('admin.articles.create',compact('article'));
    }

    public function store(Request $request){
        $article = $this->articles->store($request);

        if($article){
            Session::flash('success','Saved Successfully');
            return redirect(route('admin.articles.edit',$article->id));
        }


        Session::flash('error','There was an error. Please try again.');
        return redirect()->back();
    }

    public function update(Request $request){
        $this->articles->update($request);

        Session::flash('success','Updated Successfully');
        return redirect()->back();
    }

    public function delete($article_id){
        $this->model->where('id',$article_id)->delete();

        Session::flash('success','Deleted Successfully');
        return redirect()->back();
    }
}
