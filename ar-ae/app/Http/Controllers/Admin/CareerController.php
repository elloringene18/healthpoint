<?php

namespace App\Http\Controllers\Admin;

use App\Models\Career;
use App\Models\Insurance;
use App\Repositories\CareerManager;
use App\Repositories\InsuranceManager;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;

class CareerController extends Controller
{


    public function __construct(Career$model, CareerManager $manager)
    {
        $this->model = $model;
        $this->manager = $manager;
    }

    public function create(){
        return view('admin.careers.create');
    }

    public function edit($id){
        $data = $this->model->where('id',$id)->first();
        return view('admin.careers.create',compact('data'));
    }

    public function store(Request $request){
        $data = $this->manager->store($request);

        if($data){
            Session::flash('success','Saved Successfully');
            return redirect(route('admin.careers.edit',$data->id));
        }

        Session::flash('error','There was an error. Please try again.');
        return redirect()->back();
    }

    public function update(Request $request){
        $this->manager->update($request);

        Session::flash('success','Updated Successfully');
        return redirect()->back();
    }

    public function delete($id){
        $this->model->where('id',$id)->delete();

        Session::flash('success','Deleted Successfully');
        return redirect()->back();
    }
}
