<?php namespace App\Services\Uploaders;

use Intervention\Image\ImageManager as Image;
use Illuminate\Filesystem\Filesystem as File;

class ArticleThumbnailUploader extends Uploader implements CanUploadImage {

    use UploadsImage;

    /**
     * @var File
     */
    protected $file;

    /**
     * The target directory of the upload file.
     *
     * @var string
     */
    protected $directoryPath = 'uploads/articles';

    /**
     * Create the uploader class.
     *
     * @param Image $image
     * @param File  $file
     */
    public function __construct(Image $image,  File $file)
    {
        $this->image = $image;
        $this->file = $file;
    }

    /**
     * Get the templates to be used when uploading an image.
     *
     * @return array
     */
    protected function getImageTemplates()
    {
        return [
            [
                'name' => 'original',
                'path' => $this->directoryPath . '/images'
            ],
            [
                'name' => 'thumb',
                'path' => $this->directoryPath . '/images/thumbnails',
                'width' => 320,
                'height' => 240,
            ],
            [
                'name' => 'long',
                'path' => $this->directoryPath . '/images/long',
                'width' => 650,
                'height' => 380,
            ],
            [
                'name' => 'square',
                'path' => $this->directoryPath . '/images/square',
                'width' => 240,
                'height' => 240,
            ],
            [
                'name' => 'high',
                'path' => $this->directoryPath . '/images/high',
                'width' => 1280,
                'height' => 768,
            ]
        ];
    }
}