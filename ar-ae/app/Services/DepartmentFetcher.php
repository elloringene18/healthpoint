<?php
namespace App\Services;

use App\Models\Department;
use App\Repositories\Traits\Fetcher;

class DepartmentFetcher {

    use Fetcher;

    public function __construct(Department $model)
    {
        $this->model = $model;
    }

    public function getList()
    {
        return $this->model->orderBy('name','ASC')->lists('name','slug');
    }

    public function getListId()
    {
        return $this->model->orderBy('name','ASC')->lists('name','id');
    }
}