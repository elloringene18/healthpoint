<?php
namespace App\Services;

use App\Models\Tweet;
use App\Repositories\Traits\Fetcher;
use GuzzleHttp\Client;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Subscriber\Oauth\Oauth1;
use Mbarwick83\TwitterApi\Facades\TwitterApi;

class TweetFetcher {
    use Fetcher;

    public function __construct(Tweet $model,TwitterApi $twitterApi)
    {
        $this->model = $model;
        $this->twitter = $twitterApi;
    }

    public function getTweets()
    {

        $stack = HandlerStack::create();

        $middleware = new Oauth1([
            'consumer_key'    => 'PdwzJANlT6NFNfHqOtnOtONKx',
            'consumer_secret' => 'wX2gcpHG6MLlsX32qQ0RKDCK5WlrNd6vZ8ULeMX5huNrTxee9z',
            'token'           => '91985282-88PUeqDvX5kkkavQTc6a1ZRv3G4Si9bR8SfpT56Fp',
            'token_secret'    => '9fFI42IYddD7gpwZ0Q4BMEnvAqJVHPZiuQP99r0LaILXD'
        ]);

        $stack->push($middleware);

        $client = new Client([
            'base_uri' => 'https://api.twitter.com/1.1/',
            'handler' => $stack,
            'auth' => 'oauth',
        ]);

        $res = $client->get('statuses/user_timeline.json',['query' => [
            'screen_name' => 'HealthpointUAE',
            'count' => '10',
        ]]);

        $data = json_decode($res->getBody());

        return $data;
    }

}