<?php namespace App\Repositories;

use Illuminate\Support\Str;
use Stichoza\GoogleTranslate\TranslateClient;

trait CanCreateSlug {

    public function generateSlug($string){

        $tr = new TranslateClient(); // Default is from 'auto' to 'en'
        $tr->setSource('ar'); // Translate from English
        $tr->setTarget('en'); // Translate to Georgian

        $slugged = $tr->translate($string);

        if($slugged){

            $slug = Str::slug($slugged);
            $existFlag = true;
            $index = 1;
            $temp_slug = $slug;

            while($existFlag==true){
                $existFlag=false;
                $check = $this->model->where('slug' , $temp_slug)->first();
                if(count($check)>0) {
                    $existFlag = true;
                    $temp_slug = $slug."-".$index;
                }

                $index++;
            }
            return $temp_slug;

        }

    }
}