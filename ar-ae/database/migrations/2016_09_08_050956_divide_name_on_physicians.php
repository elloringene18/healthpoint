<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

use App\Models\Physician;

class DivideNameOnPhysicians extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('physicians',function($table){
            $table->string('lname')->after('id');
            $table->string('fname')->after('id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('physicians',function($table){
            $table->dropColumn('fname');
            $table->dropColumn('lname');
        });
    }
}
