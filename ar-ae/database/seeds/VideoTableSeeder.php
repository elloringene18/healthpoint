<?php

use Illuminate\Database\Seeder;
use App\Models\Video;

use App\Repositories\CanCreateSlug;
use Illuminate\Support\Facades\Hash;

class VideoTableSeeder extends Seeder
{
    use CanCreateSlug;

    public function __construct(Video $model)
    {
        $this->model = $model;
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'name' => 'Video Sample',
                'source' => 'videos/sample.mp4',
                'description' => "<div><p><strong>​​​Dr. Nader Darwich - Medical Director, Healthpoint​</strong></p><p>Dr. Nader Darwich, MD, serves as the Medical Director for Healthpoint and specializes in orthopedic surgery, as well as arthroscopic surgery, knee ligaments reconstruction and sports injuries.</p><p>He has been a consultant Orthopedic Surgeon in practice since 1992, and has practiced in the UAE for over 18 years. He established the Abu Dhabi Knee &amp; Sports Medicine Centre with Mubadala Healthcare in 2006 to bring world class orthopedic care to Abu Dhabi, the UAE and surrounding region. Dr. Darwich has performed over 5,500 surgeries during his time with the center.</p><p>An avid researcher, Dr. Darwich invented a device for the distal interlocking in the intramedullary nailing, which helps the operating surgeon to avoid exposure to operate at the point of distal locking.</p><p>Dr. Darwich received his medical degree from the Faculty of Medicine, Damascus University, and did his post graduate orthopedic training in France, where he practiced orthopedics until 1995.</p><p>Dr. Darwich currently holds medical licenses with the French Medical Council in orthopedics and trauma surgery, British Medical Council (Full Registration), MD with the Syrian Ministry of Health and Orthopedic Surgeon with the Syrian Ministry of Health.​​​﻿</p></div>"
            ],
            [
                'name' => 'Video Sample',
                'source' => 'videos/sample.mp4',
                'description' => "<div><p><strong>​​​Dr. Nader Darwich - Medical Director, Healthpoint​</strong></p><p>Dr. Nader Darwich, MD, serves as the Medical Director for Healthpoint and specializes in orthopedic surgery, as well as arthroscopic surgery, knee ligaments reconstruction and sports injuries.</p><p>He has been a consultant Orthopedic Surgeon in practice since 1992, and has practiced in the UAE for over 18 years. He established the Abu Dhabi Knee &amp; Sports Medicine Centre with Mubadala Healthcare in 2006 to bring world class orthopedic care to Abu Dhabi, the UAE and surrounding region. Dr. Darwich has performed over 5,500 surgeries during his time with the center.</p><p>An avid researcher, Dr. Darwich invented a device for the distal interlocking in the intramedullary nailing, which helps the operating surgeon to avoid exposure to operate at the point of distal locking.</p><p>Dr. Darwich received his medical degree from the Faculty of Medicine, Damascus University, and did his post graduate orthopedic training in France, where he practiced orthopedics until 1995.</p><p>Dr. Darwich currently holds medical licenses with the French Medical Council in orthopedics and trauma surgery, British Medical Council (Full Registration), MD with the Syrian Ministry of Health and Orthopedic Surgeon with the Syrian Ministry of Health.​​​﻿</p></div>"
            ],
            [
                'name' => 'Video Sample',
                'source' => 'videos/sample.mp4',
                'description' => "<div><p><strong>​​​Dr. Nader Darwich - Medical Director, Healthpoint​</strong></p><p>Dr. Nader Darwich, MD, serves as the Medical Director for Healthpoint and specializes in orthopedic surgery, as well as arthroscopic surgery, knee ligaments reconstruction and sports injuries.</p><p>He has been a consultant Orthopedic Surgeon in practice since 1992, and has practiced in the UAE for over 18 years. He established the Abu Dhabi Knee &amp; Sports Medicine Centre with Mubadala Healthcare in 2006 to bring world class orthopedic care to Abu Dhabi, the UAE and surrounding region. Dr. Darwich has performed over 5,500 surgeries during his time with the center.</p><p>An avid researcher, Dr. Darwich invented a device for the distal interlocking in the intramedullary nailing, which helps the operating surgeon to avoid exposure to operate at the point of distal locking.</p><p>Dr. Darwich received his medical degree from the Faculty of Medicine, Damascus University, and did his post graduate orthopedic training in France, where he practiced orthopedics until 1995.</p><p>Dr. Darwich currently holds medical licenses with the French Medical Council in orthopedics and trauma surgery, British Medical Council (Full Registration), MD with the Syrian Ministry of Health and Orthopedic Surgeon with the Syrian Ministry of Health.​​​﻿</p></div>"
            ],
            [
                'name' => 'Video Sample',
                'source' => 'videos/sample.mp4',
                'description' => "<div><p><strong>​​​Dr. Nader Darwich - Medical Director, Healthpoint​</strong></p><p>Dr. Nader Darwich, MD, serves as the Medical Director for Healthpoint and specializes in orthopedic surgery, as well as arthroscopic surgery, knee ligaments reconstruction and sports injuries.</p><p>He has been a consultant Orthopedic Surgeon in practice since 1992, and has practiced in the UAE for over 18 years. He established the Abu Dhabi Knee &amp; Sports Medicine Centre with Mubadala Healthcare in 2006 to bring world class orthopedic care to Abu Dhabi, the UAE and surrounding region. Dr. Darwich has performed over 5,500 surgeries during his time with the center.</p><p>An avid researcher, Dr. Darwich invented a device for the distal interlocking in the intramedullary nailing, which helps the operating surgeon to avoid exposure to operate at the point of distal locking.</p><p>Dr. Darwich received his medical degree from the Faculty of Medicine, Damascus University, and did his post graduate orthopedic training in France, where he practiced orthopedics until 1995.</p><p>Dr. Darwich currently holds medical licenses with the French Medical Council in orthopedics and trauma surgery, British Medical Council (Full Registration), MD with the Syrian Ministry of Health and Orthopedic Surgeon with the Syrian Ministry of Health.​​​﻿</p></div>"
            ],
        ];

        foreach ($data as $item){
            $item['category'] = 'testimonial';
            $item['slug'] = $this->generateSlug($item['name']);

            $this->model->create($item);
        }

    }
}
