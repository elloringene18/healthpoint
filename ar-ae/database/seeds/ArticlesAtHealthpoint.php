<?php

use Illuminate\Database\Seeder;
use App\Models\Article;

use App\Repositories\CanCreateSlug;

class ArticlesAtHealthpoint extends Seeder
{
    use CanCreateSlug;

    public function __construct(Article $model)
    {
        $this->model = $model;
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'title' => "Hydrotherapy",
                'content' => "<p>Healthpoint’s physiotherapy center also offers hydrotherapy, a water-based treatment that strengthens the muscles of the lower extremities and the back. Patients walk on a treadmill or stationery bicycle underwater, working against the force of the water to increase their strength. The entire treatment process is recorded to help our technicians assess your progress.</p>",
            ],
            [
                'title' => "Redcord Neurac Sling Therapy",
                'content' => "<p>Healthpoint is proud to present a unique treatment method for musculoskeletal disorders called Redcord Neurac Sling Therapy, which consists of specially tailored exercises and techniques in Redcord slings that aim to restore functional and pain free movement patterns through high levels of neuromuscular stimulation. This is the first of its kind in the UAE. Neurac treatment is pain free and with no negative side effects. It is used primarily in lower back, neck and shoulder pain.​</p>",
            ],
            [
                'title' => "Centaur",
                'content' => "<p>Healthpoint is the only facility in the MENA region to offer physiotherapy and rehabilitation treatment using the Centaur. This computer-assisted device aims to strengthen the muscles of the core and spine via stabilizing the spinal column by changing the body position within the gravitational field of the earth.</p>",
            ],
            [
                'title' => "Extremity And Wide Bore MRIs",
                'content' => "<p>Our wide-bore MRI machine, measuring at 70 centimeters in diameter, allows for maximum patient comfort and faster, high quality scans. Healthpoint also has an extremity MRI machine, the only in Abu Dhabi, that allows for imaging of the hands, wrists, ankles, elbows, knees and feet without the need for the patient to undergo a complete scan in a standard MRI machine.​​</p>",
            ],
            [
                'title' => "Vectra",
                'content' => "<p>The Vectra is a state-of-the-art scanning device used in bariatric, cosmetic and reconstructive surgery cases. It takes pictures of the patient pre-surgery and then simulates the intended results, helping patients and surgeons to better visualize the patient’s post-surgery shape. </p>",
            ],
            [
                'title' => "VelaShape",
                'content' => "<p>VelaShape has arrived at Healthpoint! Velashape is the first and only approved device by the U.S. Food and Drug Administration (FDA) to help patient reduce cellulite deposits of the abdomen, thighs, buttocks, arms, chin and other areas using a non-invasive heating treatment that literally melts fat away.​​</p>",
            ],
            [
                'title' => "Carbon Dioxide Insufflation",
                'content' => "<p>Healthpoint’s Department of Gastroenterology, unlike most other endoscopy units in the UAE, uses Co2 insufflation, which is rapidly absorbed from the gastrointestinal tract, and eliminates discomfort, bloating, and pain when the patient undergoes endoscopy.​</p>",
            ],
            [
                'title' => "Narrow Band Imaging",
                'content' => "<p>​Healthpoint also uses Narrow Band Imaging (NBI), a technology that helps to diagnose minute pre-cancerous lesions via endoscopy which could then be removed within the same appointment; this eliminates the need for surgery in most cases, and with a very low risk of complication.</p>",
            ],
            [
                'title' => "Contrast Enhanced Ultrasound",
                'content' => "<p>Healthpoint is the first hospital in the UAE to use contract enhanced ultrasound (CEUS), a sophisticated ultrasound technique that helps to diagnose abdominal and liver disorders accurately without exposing patients to radiation. This helps to keep our patients safe, while also minimizing the number of they must undergo to receive a proper diagnosis. </p>",
            ],
        ];

        foreach ($data as $item){
            $item['slug'] = $this->generateSlug($item['title']);
            $item['category_id'] = 3;

            $this->model->create($item);
        }

    }
}



