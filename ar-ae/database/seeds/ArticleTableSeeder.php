<?php

use Illuminate\Database\Seeder;
use App\Models\Article;

use App\Repositories\CanCreateSlug;

class ArticleTableSeeder extends Seeder
{
    use CanCreateSlug;

    public function __construct(Article $model)
    {
        $this->model = $model;
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $data = [
            [
                'title' => "Cricket From The Bowler To The Batsman",
                'content' => "<p>While all cricketers are prone to injury, bowlers and batsmen need to protect their bodies against different injuries given their different jobs on the field.</p>",
            ],
            [
                'title' => "Antimicrobial Resistance",
                'content' => "<p>The scale of the threat of antimicrobial resistance (AMR) has called for global action.</p>",
            ],
            [
                'title' => "All You Need to Know About Travel Vaccines",
                'content' => "<p>Packed your suitcase? Travel visa sorted? But did you stop to think about what vaccines, if any, are needed to keep you safe during your travels?</p>",
            ],
            [
                'title' => "Cricket From The Bowler To The Batsman",
                'content' => "<p>While all cricketers are prone to injury, bowlers and batsmen need to protect their bodies against different injuries given their different jobs on the field.</p>",
            ],
            [
                'title' => "Antimicrobial Resistance",
                'content' => "<p>The scale of the threat of antimicrobial resistance (AMR) has called for global action.</p>",
            ],
            [
                'title' => "All You Need to Know About Travel Vaccines",
                'content' => "<p>Packed your suitcase? Travel visa sorted? But did you stop to think about what vaccines, if any, are needed to keep you safe during your travels?</p>",
            ],
            [
                'title' => "Cricket From The Bowler To The Batsman",
                'content' => "<p>While all cricketers are prone to injury, bowlers and batsmen need to protect their bodies against different injuries given their different jobs on the field.</p>",
            ],
            [
                'title' => "Antimicrobial Resistance",
                'content' => "<p>The scale of the threat of antimicrobial resistance (AMR) has called for global action.</p>",
            ],
            [
                'title' => "All You Need to Know About Travel Vaccines",
                'content' => "<p>Packed your suitcase? Travel visa sorted? But did you stop to think about what vaccines, if any, are needed to keep you safe during your travels?</p>",
            ],
            [
                'title' => "Cricket From The Bowler To The Batsman",
                'content' => "<p>While all cricketers are prone to injury, bowlers and batsmen need to protect their bodies against different injuries given their different jobs on the field.</p>",
            ],
            [
                'title' => "Antimicrobial Resistance",
                'content' => "<p>The scale of the threat of antimicrobial resistance (AMR) has called for global action.</p>",
            ],
            [
                'title' => "All You Need to Know About Travel Vaccines",
                'content' => "<p>Packed your suitcase? Travel visa sorted? But did you stop to think about what vaccines, if any, are needed to keep you safe during your travels?</p>",
            ],
            [
                'title' => "Cricket From The Bowler To The Batsman",
                'content' => "<p>While all cricketers are prone to injury, bowlers and batsmen need to protect their bodies against different injuries given their different jobs on the field.</p>",
            ],
            [
                'title' => "Antimicrobial Resistance",
                'content' => "<p>The scale of the threat of antimicrobial resistance (AMR) has called for global action.</p>",
            ],
            [
                'title' => "All You Need to Know About Travel Vaccines",
                'content' => "<p>Packed your suitcase? Travel visa sorted? But did you stop to think about what vaccines, if any, are needed to keep you safe during your travels?</p>",
            ],
            [
                'title' => "Cricket From The Bowler To The Batsman",
                'content' => "<p>While all cricketers are prone to injury, bowlers and batsmen need to protect their bodies against different injuries given their different jobs on the field.</p>",
            ],
            [
                'title' => "Antimicrobial Resistance",
                'content' => "<p>The scale of the threat of antimicrobial resistance (AMR) has called for global action.</p>",
            ],
            [
                'title' => "Top Tips For Cycling",
                'content' => "<p>Are you an avid cycler? Keep yourself healthy with our top tips for anyone from the weekend cycler to the mountain biker and competing triathlete.</p>",
            ],
        ];

        foreach ($data as $item){
            $item['slug'] = $this->generateSlug($item['title']);
            $item['category_id'] = 4;

            $this->model->create($item);
        }

    }
}



