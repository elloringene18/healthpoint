<div class="help-box clearfix brownish-bg">
    <h1>دعنا نساعدك</h1>
    <ul id="help-icon-list">
        <li><a href="mailto:info@healthpoint.ae"><span class="icon-text phone">إتصل بنا</span></a></li>
        <li><a href="{{ url('request-appointment') }}"><span class="icon-text calendar">احجز موعد</span></a> </li>
        <li><a href="{{ url('find-a-doctor') }}"><span class="icon-text doctor">البحث عن طبيب</span></a> </li>
        <li><a href="{{ url('clinical-services') }}"><span class="icon-text search">البحث عن خدمة</span></a> </li>
        <li><a href="{{ url('patients/insurance') }}"><span class="icon-text insurance">معلومات حول التأمين</span></a> </li>
    </ul>
</div>