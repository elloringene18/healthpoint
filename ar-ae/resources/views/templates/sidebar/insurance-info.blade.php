<div class="clearfix widget">
    <h1 class="panel-title">معلومات حول التأمين</h1>


    @inject('optionFetcher','App\Services\OptionFetcher')

    <div class="col-md-12">
        <div class="row padding-t-0">
            <div class="panel-group" id="accordion">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapse1" class="collapsed">
                                أسئلة عامة</a>
                        </h4>
                    </div>
                    <div id="collapse1" class="panel-collapse collapse">
                        <div class="panel-body">
                            {!! str_replace("&nbsp;"," ",$optionFetcher->getBySlug('insurance-general')) !!}

                        </div>
                    </div>
                </div>



                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapse2" class="collapsed">
                                الخدمات الداخلية</a>
                        </h4>
                    </div>
                    <div id="collapse2" class="panel-collapse collapse">
                        <div class="panel-body">
                            {!! str_replace("&nbsp;"," ",$optionFetcher->getBySlug('insurance-in')) !!}

                        </div>
                    </div>
                </div>



                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapse3">
                                الخدمات الخارجية</a>
                        </h4>
                    </div>
                    <div id="collapse3" class="panel-collapse collapse in">
                        <div class="panel-body">
                            {!! str_replace("&nbsp;"," ",$optionFetcher->getBySlug('insurance-out')) !!}
                        </div>
                    </div>
                </div>



                </div>
            </div>
        </div>
    </div>
</div>