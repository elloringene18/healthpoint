<div class="widget">
    <h1 class="brown-header">الأكثر شعبية</h1>
    <div  class="padding-l-0 padding-r-0">

        @inject('articleFetcher', 'App\Services\ArticleFetcher')

        <?php $articles = $articleFetcher->getByCategoryPopular(4,10); ?>

        <ul class="popular-list clearfix">
            @foreach($articles as $index=>$article)
                <li class="col-md-12 col-sm-6 col-xs-6">
                    <div class="col-md-1 col-sm-1 col-xs-1 padding-r-0">
                        <div class="row text-left">
                            <span class="rank">{{ $index+1 }}</span>
                        </div>
                    </div>
                    <div class="col-md-11 col-sm-9 col-xs-9 padding-l-0 padding-r-25">
                        <div class="row">
                            <a href="{{ route('blog.single',$article->slug) }}"><h1>{{ $article->title }}</h1></a>
                        </div>
                    </div>
                </li>
            @endforeach
        </ul>
    </div>
</div>