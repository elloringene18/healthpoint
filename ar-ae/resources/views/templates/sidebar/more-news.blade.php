<div class="widget">
    <h1 class="panel-title">المزيد من الأخبار</h1>
    <div  class="padding-l-0 padding-r-0">
        @inject('articleFetcher', 'App\Services\ArticleFetcher')

        <?php $articles = $articleFetcher->getByCategory(5,5); ?>

        <ul class="post-list clearfix">
            @forelse($articles as $article)
                <li class="col-md-12 col-sm-6 col-xs-6">
                    <div class="col-md-3 col-sm-5 col-xs-5 padding-r-0">
                        <div class="row text-left">
                            <a href="{{ route('blog.single',$article->slug) }}"><img src="{{ $article->thumbnailSquare }}" width="100%"></a>
                        </div>
                    </div>
                    <div class="col-md-9 col-sm-7 col-xs-7 padding-l-0 padding-r-25">
                        <a href="{{ route('blog.single',$article->slug) }}"><h1>{{ $article->title }}</h1></a>
                    </div>
                </li>
            @empty
            @endforelse
        </ul>
    </div>
</div>
