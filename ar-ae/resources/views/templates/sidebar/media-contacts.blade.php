@inject('optionFetcher','App\Services\OptionFetcher')

<?php
    $contact = $optionFetcher->getBySlug('media-contact');
?>

<div class="widget">
    <h1 class="panel-title">جهات الاتصال لوسائل الاعلام</h1>
    <div  class="padding-l-0 padding-r-0">
        {!! $contact ? $contact : '' !!}
    </div>
</div>