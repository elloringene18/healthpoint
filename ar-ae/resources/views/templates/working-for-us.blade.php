@inject('optionFetcher','App\Services\OptionFetcher')

<?php
$working = $optionFetcher->getBySlug('working-for-us');
?>

<div class="banner-box about video working-for-us">

    @inject('optionFetcher','App\Services\OptionFetcher')

    @if($optionFetcher->getBySlug('working-for-us-banner-image'))
        <div class="videoWrapper work-with-us" style="background-image: url({{ asset('public'.$optionFetcher->getBySlug('working-for-us-banner-image')) }})">
    @else
        <div class="videoWrapper work-with-us">
    @endif
        <a href="{{ $optionFetcher->getBySlug('working-for-us-video') ? $optionFetcher->getBySlug('working-for-us-video') : asset('public/videos/Working at Healthpoint.mp4') }}" class="html5lightbox cover"  data-width="780" data-height="480" title="Working at Healthpoint">&nbsp;</a>
    </div>

    <div class="panel text-center margin-t-5">
        <h1>الانضمام إلى فريق العمل بهيلث بوينت</h1>
        {!! $working ? $working : '' !!}
    </div>
</div>