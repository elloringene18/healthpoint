@extends('admin.master')

@section('content')
        <!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Working for us
        <small>Control panel</small>
    </h1>
</section>

<!-- Main content -->
<section class="content">
    <div class="box box-info">
        <div class="box-header">
            <div class="col-md-12">
                @if(Session::has('success'))
                    <div class="alert alert-success">
                        {{ Session::get('success') }}
                    </div>
                @endif

                <h3 class="box-title">Settings</h3>

                <!-- /. tools -->
            </div>
        </div>
        <!-- /.box-header -->

        @inject('optionFetcher','App\Services\OptionFetcher')

        <?php
        $options = $optionFetcher->getAll();
        ?>
        <div class="row">
            <div class="col-md-12">
                <div class="box-body pad">
                    {!! Form::open(['route'=> 'admin.options.update','files'=>true]) !!}

                    <div class="col-md-12">
                        <div class="form-group">
                            <label>Banner Image:</label>
                            <img src="{!! isset($options['working-for-us-banner-image']) ? asset('public'.$options['working-for-us-banner-image']->value) : '' !!}" width="100%">
                            <input type="file" class="form-control" name="working-for-us-banner-image" value="">
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="form-group">
                            <label>Video:</label>
                            <input type="text" name="working-for-us-video" value="{!! isset($options['working-for-us-video']) ? $options['working-for-us-video']->value : '' !!}" class="form-control">
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="form-group">
                            <label>Working for us:</label>
                            <textarea class="editor" name="working-for-us" rows="10" cols="80">{!! isset($options['working-for-us']) ? $options['working-for-us']->value : '' !!}</textarea>
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="form-group">
                            <h4>Physician recruitment</h4>
                            <textarea class="editor" name="working-for-us-recruit" rows="3" cols="80">{!! isset($options['working-for-us-recruit']) ? $options['working-for-us-recruit']->value : '' !!}</textarea>
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="form-group">
                            <h4>Featured Physician</h4>
                            @inject('pFetcher','App\Services\PhysicianFetcher')

                            <?php
                                $physicians = $pFetcher->getList();
                            ?>
                            {!! Form::select('featured-physician',$physicians,isset($options['featured-physician']) ? $options['featured-physician']->value : null,['class'=>'form-control']) !!}
                        </div>
                    </div>



                    <div class="col-md-12">
                        <h2>Emiratization</h2>
                        <div class="form-group">
                            <label>Video:</label>
                            <input type="text" name="emiratization-video" value="{!! isset($options['emiratization-video']) ? $options['emiratization-video']->value : '' !!}" class="form-control">
                        </div>
                        <div class="form-group">
                            <label>Video 2:</label>
                            <input type="text" name="emiratization-video-2" value="{!! isset($options['emiratization-video-2']) ? $options['emiratization-video-2']->value : '' !!}" class="form-control">
                        </div>
                        <div class="form-group">
                            <h4>Text</h4>
                            <textarea class="editor" name="emiratization" rows="3" cols="80">{!! isset($options['emiratization']) ? $options['emiratization']->value : '' !!}</textarea>
                        </div>
                    </div>



                    <div class="col-md-12">
                        <h2>Opportunities</h2>
                        <div class="form-group">
                            <h4>Top Text</h4>
                            <textarea class="editor" name="opportunities-top" rows="3" cols="80">{!! isset($options['opportunities-top']) ? $options['opportunities-top']->value : '' !!}</textarea>
                        </div>
                        <div class="form-group">
                            <h4>Bottom Text</h4>
                            <textarea class="editor" name="opportunities-bottom" rows="3" cols="80">{!! isset($options['opportunities-bottom']) ? $options['opportunities-bottom']->value : '' !!}</textarea>
                        </div>
                    </div>


                    <div class="col-md-12">
                        <div class="form-group">
                            <input type="submit" class="form-control">
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</section>
<!-- /.content -->

@endsection

@section('custom-js')
    <script>
        $('.datepicker').datepicker();
    </script>
@endsection