@extends('admin.master')

@section('content')
        <!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        General Settings
        <small>Control panel</small>
    </h1>
</section>

<!-- Main content -->
<section class="content">
    <div class="box box-info">
        <div class="box-header">
            <div class="col-md-12">
                @if(Session::has('success'))
                    <div class="alert alert-success">
                        {{ Session::get('success') }}
                    </div>
                @endif

                <h3 class="box-title">Settings</h3>

                <!-- /. tools -->
            </div>
        </div>
        <!-- /.box-header -->

        @inject('optionFetcher','App\Services\OptionFetcher')

        <?php
        $options = $optionFetcher->getAll();

        ?>
        <div class="row">
            <div class="col-md-12">
                <div class="box-body pad">
                    {!! Form::open(['route'=> 'admin.options.update','files'=>true]) !!}

                    <div class="col-md-12">
                        <div class="form-group">
                            <label>Site title</label>
                            <input type="text" name="site-title" required class="form-control" value="{!! isset($options['site-title']) ? $options['site-title']->value: '' !!}">
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="form-group">
                            <label>Home Banner Image:</label>
                            <img src="{!! isset($options['home-banner-image']) ? asset('public'.$options['home-banner-image']->value) : '' !!}" width="100%">
                            <input type="file" class="form-control" name="home-banner-image" value="">
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Clinic Hours:</label>
                            <textarea class="editor cke_rtl" name="clinic-hours" rows="10" cols="80">{!! isset($options['clinic-hours']) ? $options['clinic-hours']->value : '' !!}</textarea>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Location:</label>
                            <textarea class="editor cke_rtl" name="location" rows="10" cols="80">{!! isset($options['location']) ? $options['location']->value : '' !!}</textarea>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Contact:</label>
                            <textarea class="editor cke_rtl" name="contact" rows="10" cols="80">{!! isset($options['contact']) ? $options['contact']->value : '' !!}</textarea>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Media Contact:</label>
                            <textarea class="editor cke_rtl" name="media-contact" rows="10" cols="80">{!! isset($options['media-contact']) ? $options['media-contact']->value : '' !!}</textarea>
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="form-group">
                            <input type="submit" class="form-control">
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</section>
<!-- /.content -->

@endsection

@section('custom-js')
    <script>
        $('.datepicker').datepicker();
    </script>
@endsection