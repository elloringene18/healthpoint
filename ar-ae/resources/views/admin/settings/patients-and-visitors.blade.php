@extends('admin.master')

@section('content')
        <!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Patient and Visitors
        <small>Control panel</small>
    </h1>
</section>

<!-- Main content -->
<section class="content">
    <div class="box box-info">
        <div class="box-header">
            <div class="col-md-12">
                @if(Session::has('success'))
                    <div class="alert alert-success">
                        {{ Session::get('success') }}
                    </div>
                @endif

                <h3 class="box-title">Settings</h3>

                <!-- /. tools -->
            </div>
        </div>
        <!-- /.box-header -->

        @inject('optionFetcher','App\Services\OptionFetcher')

        <?php
            $options = $optionFetcher->getAll();
        ?>
        <div class="row">
            <div class="col-md-12">
                <div class="box-body pad">
                    {!! Form::open(['route'=> 'admin.options.update','files'=>true]) !!}

                    <div class="col-md-12">
                        <div class="form-group">
                            <label>Banner Image:</label>
                            <img src="{!! isset($options['patients-banner-image']) ? asset('public'.$options['patients-banner-image']->value) : '' !!}" width="100%">
                            <input type="file" class="form-control" name="patients-banner-image" value="">
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="form-group">
                            <label>Our Services:</label>
                            <textarea class="editor" name="our-services" rows="3" cols="80">{!! isset($options['our-services']) ? $options['our-services']->value : '' !!}</textarea>
                        </div>
                    </div>


                    <div class="col-md-12">
                        <div class="form-group">
                            <h2>Insurance Info</h2>
                            <label>Insurance Partners:</label>
                            <textarea class="editor" name="insurance-partners" rows="3" cols="80">{!! isset($options['insurance-partners']) ? $options['insurance-partners']->value : '' !!}</textarea>
                        </div>
                    </div>


                    <div class="col-md-12">
                        <div class="form-group">
                            <label>General:</label>
                            <textarea class="editor" name="insurance-general" rows="3" cols="80">{!! isset($options['insurance-general']) ? $options['insurance-general']->value : '' !!}</textarea>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>In-patient Services:</label>
                            <textarea class="editor" name="insurance-in" rows="3" cols="80">{!! isset($options['insurance-in']) ? $options['insurance-in']->value : '' !!}</textarea>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>Out-patient Services:</label>
                            <textarea class="editor" name="insurance-out" rows="3" cols="80">{!! isset($options['insurance-out']) ? $options['insurance-out']->value : '' !!}</textarea>
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="form-group">
                            <h2>Patient Rights</h2>
                            <label>Healthpoint patients and their families have the right:</label>
                            <textarea class="editor" name="patient-rights" rows="3" cols="80">{!! isset($options['patient-rights']) ? $options['patient-rights']->value : '' !!}</textarea>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>Healthpoint patients and their families are responsible to:</label>
                            <textarea class="editor" name="patient-responsibilities" rows="3" cols="80">{!! isset($options['patient-responsibilities']) ? $options['patient-responsibilities']->value : '' !!}</textarea>
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="form-group">
                            <input type="submit" class="form-control">
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</section>
<!-- /.content -->

@endsection

@section('custom-js')
    <script>
        $('.datepicker').datepicker();
    </script>
@endsection