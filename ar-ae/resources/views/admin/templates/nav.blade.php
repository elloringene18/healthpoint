<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu">
            <li class="header">MAIN NAVIGATION</li>
            <li>
                <a target="_blank" href="{{ route('home') }}">
                    <i class="fa fa-location-arrow"></i> <span>Visit Site</span>
                </a>
            </li>
            <li class="{{ str_contains(Request::path(), '/articles') ? 'active' : '' }} treeview">
                <a href="#">
                <i class="fa fa-pencil"></i> <span>Posts</span>
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
                </a>
                <ul class="treeview-menu">
                    @inject('catFetcher','App\Services\CategoryFetcher')

                    <?php
                        $categories = $catFetcher->getAll();
                    ?>
                    @foreach($categories as $category)
                        @if($category->id != 6)
                            <li><a href="{{ route('admin.articles.get',$category->id) }}"><i class="fa fa-circle-o"></i> {{ $category->name }}</a></li>
                        @endif
                    @endforeach
                </ul>
            </li>
            {{--<li>--}}
                {{--<a href="{{ route('admin.articles.index') }}">--}}
                    {{--<i class="fa fa-dashboard"></i> <span>Articles</span>--}}
                {{--</a>--}}
            {{--</li>--}}
            <li class="{{ str_contains(Request::path(), '/physicians') ? 'active' : '' }}">
                <a href="{{ route('admin.physicians.index') }}">
                    <i class="fa fa-group"></i> <span>Physicians</span>
                </a>
            </li>
            <li class="{{ str_contains(Request::path(), '/departments') ? 'active' : '' }}">
                <a href="{{ route('admin.departments.index') }}">
                    <i class="fa fa-star-o"></i> <span>Departments</span>
                </a>
            </li>
            <li class="{{ str_contains(Request::path(), '/insurances') ? 'active' : '' }}">
                <a href="{{ route('admin.insurances.index') }}">
                    <i class="fa fa-umbrella"></i> <span>Insurance</span>
                </a>
            </li>

            <li class="treeview {{ str_contains(Request::path(), '/galleries') ? 'active' : '' }}">
                <a href="#">
                    <i class="fa fa-file-image-o"></i> <span>Gallery</span>
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{ route('admin.galleries.filter','photo') }}"><i class="fa fa-circle-o"></i> Photo Albums</a></li>
                    <li><a href="{{ route('admin.galleries.filter','video') }}"><i class="fa fa-circle-o"></i> Video Albums</a></li>
                </ul>
            </li>
            <li class="{{ str_contains(Request::path(), '/management') ? 'active' : '' }}">
                <a href="{{ route('admin.management.index') }}">
                    <i class="fa  fa-briefcase"></i> <span>Management</span>
                </a>
            </li>
            <li class="{{ str_contains(Request::path(), '/events') ? 'active' : '' }}">
                <a href="{{ route('admin.events.index') }}">
                    <i class="fa fa-calendar-check-o"></i> <span>Events</span>
                </a>
            </li>
            <li class="{{ str_contains(Request::path(), '/uploads') ? 'active' : '' }}">
                <a href="{{ route('admin.uploads.index') }}">
                    <i class="fa fa-calendar-check-o"></i> <span>Uploads</span>
                </a>
            </li>
            {{--<li class="{{ str_contains(Request::path(), '/videos') ? 'active' : '' }}">--}}
                {{--<a href="{{ route('admin.videos.index') }}">--}}
                    {{--<i class="fa fa-dashboard"></i> <span>Video</span>--}}
                {{--</a>--}}
            {{--</li>--}}
            <li class="{{ str_contains(Request::path(), '/appointments') ? 'active' : '' }}">
                <a href="{{ route('admin.appointments.index') }}">
                    <i class="fa fa-book"></i> <span>Appointment Requests</span>
                </a>
            </li>
            <li class="{{ str_contains(Request::path(), '/careers') ? 'active' : '' }}">
                <a href="{{ route('admin.careers.index') }}">
                    <i class="fa  fa-key"></i> <span>Careers</span>
                </a>
            </li>
            <li class="treeview {{ str_contains(Request::path(), '/surveys') ? 'active' : '' }}">
                <a href="{{ route('admin.surveys.index') }}">
                    <i class="fa fa-dashboard"></i> <span>Surveys</span>
                    <span class="pull-right-container">
                      <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                {{--<ul class="treeview-menu">--}}
                    {{--<li><a href="{{ route('admin.options.index') }}"><i class="fa fa-circle-o"></i> General</a></li>--}}
                    {{--<li><a href="{{ route('admin.options.about') }}"><i class="fa fa-circle-o"></i> About Us</a></li>--}}
                    {{--<li><a href="{{ route('admin.options.patients') }}"><i class="fa fa-circle-o"></i> Patient and Visitors</a></li>--}}
                    {{--<li><a href="{{ route('admin.options.working') }}"><i class="fa fa-circle-o"></i> Working for us</a></li>--}}
                {{--</ul>--}}
            </li>
            <li class="treeview {{ str_contains(Request::path(), '/options') ? 'active' : '' }}">
                <a href="#">
                    <i class="fa fa-dashboard"></i> <span>Site Settings</span>
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{ route('admin.options.index') }}"><i class="fa fa-circle-o"></i> General</a></li>
                    <li><a href="{{ route('admin.options.about') }}"><i class="fa fa-circle-o"></i> About Us</a></li>
                    <li><a href="{{ route('admin.options.patients') }}"><i class="fa fa-circle-o"></i> Patient and Visitors</a></li>
                    <li><a href="{{ route('admin.options.working') }}"><i class="fa fa-circle-o"></i> Working for us</a></li>
                </ul>
            </li>

            {{--<li class="{{ str_contains(Request::path(), '/tweets') ? 'active' : '' }}">--}}
                {{--<a href="{{ route('admin.tweets.index') }}">--}}
                    {{--<i class="fa fa-dashboard"></i> <span>Tweets</span>--}}
                {{--</a>--}}
            {{--</li>--}}

            <li>
                <a href="{{ route('admin.logout') }}">
                    <i class="fa fa-mail-reply"></i> <span>Logout</span>
                </a>
            </li>

            {{--<li class="treeview">--}}
                {{--<a href="#">--}}
                    {{--<i class="fa fa-dashboard"></i> <span>Gallery</span>--}}
                    {{--<span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>--}}
                {{--</a>--}}
                {{--<ul class="treeview-menu">--}}
                    {{--<li><a href="index.html"><i class="fa fa-circle-o"></i> Photos</a></li>--}}
                    {{--<li><a href="index2.html"><i class="fa fa-circle-o"></i> Videos</a></li>--}}
                {{--</ul>--}}
            {{--</li>--}}

        </ul>
    </section>
    <!-- /.sidebar -->
</aside>