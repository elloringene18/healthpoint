@extends('admin.master')

@section('content')
        <!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Galleries
        <small>Control panel</small>
    </h1>
</section>

<!-- Main content -->
<section class="content">
    <div class="box box-info">
        <div class="box-header">
            <div class="col-md-12">
            @if(Session::has('success'))
                <div class="alert alert-success">
                    {{ Session::get('success') }}
                </div>
            @endif

            <h3 class="box-title">{{ isset($data) ? 'Edit' : 'Add' }} Gallery</h3>

            <!-- /. tools -->
            </div>
        </div>
        <!-- /.box-header -->

        <div class="row">
            <div class="col-md-12">
                <div class="box-body pad">
                    {!! Form::open(['route'=> isset($data) ? 'admin.galleries.update' : 'admin.galleries.store','files'=>true]) !!}

                    @if(isset($data))
                        <div class="col-md-12">
                            <img src="{{ $data->thumbnail }}" height="100">
                            <br>
                            Remove Thumbnail &nbsp;<input type="checkbox" name="remove_thumb">
                            <br>
                            <br>
                        </div>
                    @endif

                        @if(isset($data))
                            <input type="hidden" name="id" value="{{ $data->id }}">
                        @endif
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Name</label>
                                <input type="text" name="title" class="form-control" value="{{ isset($data) ? $data->title : '' }}">
                            </div>
                            <div class="form-group">

                                <label>Thumbnail</label>
                                <input type="file" class="form-control"  value="" name="thumbnail">
                            </div>
                            <div class="form-group">
                                <?php
                                    $types = [
                                        'photo' => 'Photo',
                                        'video' => 'Video'
                                    ]
                                ?>
                                <label>Type</label>
                                {!! Form::select('type',$types, isset($data) ? $data->type : 'photo',['class'=>'form-control']) !!}
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Description (Optional)</label>
                                <textarea class="editor" name="description" rows="10" cols="80">{{ isset($data) ? $data->description : '' }}</textarea>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Photos</label>
                                <input type="file" class="form-control" multiple value="" name="photos[]">
                            </div>
                        </div>

                        @if(isset($data))
                            <div class="col-md-12">
                                @foreach($data->contents as $photo)
                                    @if(isset($photo->uploads[0]))
                                        <div class="col-md-3 relative">
                                            @if(strpos($photo->uploads[0]->mime_type, 'image')!== false)
                                                <img src="{{ $photo->thumbnail }}" width="100%" style="margin:5px;">
                                            @elseif(strpos($photo->uploads[0]->mime_type, 'video')!== false)
                                                <div class="pull-left" style="margin: 5px">
                                                    <video width="100%" controls preload="metadata">
                                                        <source src="{{ asset('public').$photo->uploads[0]->url }}#t=4" type="video/mp4">
                                                        Your browser does not support the video tag.
                                                    </video>
                                                </div>
                                            @else
                                                <li>Unknown File</li>
                                            @endif
                                            <p style="font-size: 10px">{{ $photo->name }}</p>
                                            <a href="{{ route('admin.galleries.delete-item',$photo->id) }}" style="position: absolute;top: 0;right: 0;background: #fff;padding: 5px;">Delete</a>
                                        </div>
                                    @endif
                                @endforeach
                            </div>
                        @endif

                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Post Date</label>
                                <input type="text"  name="created_at" value="{{ isset($data) ? $data->created_at->format('m/d/Y') : \Carbon\Carbon::now()->format('m/d/Y') }}" class="datepicker form-control">
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="form-group">
                                <input type="submit" class="form-control">
                            </div>
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</section>
<!-- /.content -->

@endsection


@section('custom-js')
    <script>
        $('.datepicker').datepicker();
    </script>
@endsection