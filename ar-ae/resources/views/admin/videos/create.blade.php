@extends('admin.master')

@section('content')
        <!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Videos
        <small>Control panel</small>
    </h1>
</section>

<!-- Main content -->
<section class="content">
    <div class="box box-info">
        <div class="box-header">
            <div class="col-md-12">
            @if(Session::has('success'))
                <div class="alert alert-success">
                    {{ Session::get('success') }}
                </div>
            @endif

            <h3 class="box-title">{{ isset($data) ? 'Edit' : 'Add' }} Video</h3>

            <!-- /. tools -->
            </div>
        </div>
        <!-- /.box-header -->

        <div class="row">
            <div class="col-md-12">
                <div class="box-body pad">
                    {!! Form::open(['route'=> isset($data) ? 'admin.videos.update' : 'admin.videos.store','files'=>true]) !!}


                    @if(isset($data))
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-6">
                                    Video: <br>
                                    <video width="100%" controls>
                                        <source src="{{ asset('public').$data->source }}" type="video/mp4">
                                        Your browser does not support the video tag.
                                    </video>
                                </div>

                                <div class="col-md-6">
                                    Thumbnail: <br>
                                    <img src="{{ $data->thumbnail }}" width="85%">
                                    <br>
                                    Remove Thumbnail &nbsp;<input type="checkbox" name="remove_thumb">
                                    <br>
                                    <br>
                                </div>
                            </div>
                        </div>

                    @endif

                        @if(isset($data))
                            <input type="hidden" name="id" value="{{ $data->id }}">
                        @endif
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Name</label>
                                <input type="text" name="name" class="form-control" value="{{ isset($data) ? $data->name : '' }}">
                            </div>

                            <div class="form-group">
                                <?php
                                    $types = [
                                        'testimonial' => 'Testimonial',
                                    ]
                                ?>
                                <label>Type</label>
                                {!! Form::select('category',$types, isset($data) ? $data->category : 'testimonial',['class'=>'form-control']) !!}
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Description (Optional)</label>
                                <textarea class="editor" name="description" rows="10" cols="80">{{ isset($data) ? $data->description : '' }}</textarea>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Video</label>
                                <input type="file" class="form-control" multiple value="" name="video">
                            </div>
                        </div>

                        <div class="col-md-12">

                            <div class="form-group">
                                <label>Thumbnail</label>
                                <input type="file" class="form-control"  value="" name="photo">
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="form-group">
                                <input type="submit" class="form-control">
                            </div>
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</section>
<!-- /.content -->

@endsection