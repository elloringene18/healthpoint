@inject('optionFetcher','App\Services\OptionFetcher')

<?php
$clinicHours = $optionFetcher->getBySlug('clinic-hours');
$location = $optionFetcher->getBySlug('location');
$contact = $optionFetcher->getBySlug('contact');
?>

<h1 class="panel-title">معلومات حول المستشفى</h1>

<h3 class="standard-bold">أوقات عمل العيادات الخارجية</h3>
{!! $clinicHours ? $clinicHours : 'N/A' !!}

<hr class="brown">

<h3 class="standard-bold">الموقع</h3>
{!! $location ? $location : 'N/A' !!}

<hr class="brown">

<h3 class="standard-bold">إتصل بنا:</h3>
{!! $contact ? $contact : 'N/A' !!}

