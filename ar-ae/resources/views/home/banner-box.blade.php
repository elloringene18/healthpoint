@inject('departmentFetcher','App\Services\DepartmentFetcher')
@inject('optionFetcher','App\Services\OptionFetcher')
<?php
    $departments = $departmentFetcher->getList();
?>

<div class="banner-box" style="background-image: url({{ $optionFetcher->getBySlug('home-banner-image') ? asset('public'.$optionFetcher->getBySlug('home-banner-image')) : asset('public/images/Home-Screen-Image.png') }}) ">
    <div class="col-md-4 col-sm-4 col-xs-6">
        <div class="panel text-center">
            <img src="{{ asset('public/images/icons/doctor-white.png') }}" height="40">
            <h1>البحث عن طبيب</h1>
            {!! Form::open(['route'=>'physician.index','method'=>'get']) !!}
                <input type="text" placeholder="البحث عن طبيب" name="name" class="form-control icon-input search margin-b-0">
                <hr>
                <div class="form-group">
                    {!! Form::select('specialty',$departments,null,['class'=>'selectpicker form-control']) !!}
                </div>
                <div class="">
                    <select class="selectpicker form-control" name="gender">
                        <option value="any">جنس الطبيب</option>
                        <option value="M"> طبيب</option>
                        <option value="F"> طبيبة</option>
                    </select>
                </div>
                <input type="submit" class="no-bg white" value="بحث">
            {!! Form::close() !!}
        </div>
    </div>
    <div class="col-md-4 col-sm-4 col-xs-6">
        <div class="panel text-center">
            <img src="{{ asset('public/images/icons/search-white.png') }}" height="40">
            <h1>ما نوع الخدمة</h1>

            {!! Form::open(['route'=>'search.service']) !!}
                <div class="form-group">
                    <input type="text" placeholder="البحث عن خدمة" name="keyword" class="icon-input search form-control">
                </div>
                <p class="text-left">البحث عن خدمة</p>
            {!! Form::close() !!}
        </div>
    </div>
</div>