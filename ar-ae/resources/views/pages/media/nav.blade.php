<div class="col-md-12">
    <div class="row">
        <ul class="border-line-list col-4 clearfix xshort">
            <li class="col-md-4 col-xs-12 {{ str_contains(Request::path(), 'news') ? 'active' : '' }}"><a href="{{ url('media/news') }}">أخبار </a></li>
{{--            <li class="col-md-3 col-xs-12 {{ str_contains(Request::path(), 'seminars-and-workshops') ? 'active' : '' }}"><a href="{{ url('media/seminars-and-workshops') }}">الندوات وورش العمل</a></li>--}}
            <li class="col-md-4 col-xs-12 {{ str_contains(Request::path(), 'hospital-campaigns') ? 'active' : '' }}"><a href="{{ url('media/hospital-campaigns') }}">حملات المستشفى</a></li>
            <li class="col-md-4 col-xs-12 {{ str_contains(Request::path(), 'galleries') ? 'active' : '' }}"><a href="{{ url('media/galleries') }}">المعارض</a></li>
        </ul>
    </div>
</div>