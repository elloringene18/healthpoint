@include('header')
<div class="pull-left">
    <div class="row">
        <div class="page col-md-9 main-content">
            <h1 class="panel-title">وسائل الإعلام</h1>

            @include('templates.media-banner')

            <div class="col-md-12">
                <div class="row">

                    @include('pages.media.nav')

                    <h1 class="panel-title">معرض الفيديوهات والصور</h1>
                    <p>شاهد الأطباء والموظفين وهم يشاركون في العديد من الفعاليات الاجتماعية منذ افتتاح المستشفى عام 2013.</p>
                    <div class="row padding-t-0">
                        <ul class="float-list gallery cols-3">
                            @foreach($galleries as $index=>$gallery)

                                @if($index%3==0)
                                    <div class="clearfix">
                                @endif
                                    <li class="col-md-4 col-sm-6 col-xs-12">
                                        <div class="wrap clearfix">
                                            <a href="{{ route('pages.media.album.single',$gallery->slug) }}">
                                                <img src="{{ $gallery->thumbnail }}" width="100%">
                                                <h1 class="margin-t-8">{{ $gallery->title }}</h1>
                                            </a>
{{--                                            <span class="date">{{ $gallery->created_at->format('M d Y') }}</span>--}}
                                        </div>
                                    </li>
                                @if($index%3==2)
                                    </div>
                                @endif

                            @endforeach
                        </ul>
                    </div>

                    <div class="pagination-wrap text-center">
                        @include('pagination.default', ['paginator' => $galleries])
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-3 sidebar">
            @include('templates.sidebar.help')
            @include('templates.sidebar.media-contacts')
            @include('templates.sidebar.media-pack')
            @include('templates.sidebar.tweets')
        </div>
    </div>
</div>

@section('custom-js')
    <script>
    </script>
@endsection

@include('footer')


