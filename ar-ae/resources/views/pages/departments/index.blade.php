@section('custom-styles-alt')
    <style type="text/css">
        .main-content p {
            min-height: 1px;
        }
    </style>
@endsection

@include('header')

<div class="pull-left">
    <div class="row">
        <div class="col-md-9">
            <h1 class="panel-title">الخدمات والأقسام السريرية</h1>
            <p>من خلال نموذج الرعاية الصحية المتكامل في "هيلث بوينت"، يتعاون الأطباء من مختلف الأقسام على توفير الرعاية الصحية المتكاملة للمرضى.</p>
            <div class="col-md-3 sidebar col-xs-right-0">
                <div class="row">
                    @include('pages.departments.sidebar')
                </div>
            </div>
            <div class="col-md-9 main-content padding-r-0 page col-xs-left-0 col-xs-right-0">
                <div class="col-md-12">
                    <div class="row">
                        @if($department)
                            <h1 class="panel-title">{{ $department->name }}</h1>
                            {!! $department->description !!}
                        @else
                            <h1 class="panel-title">لم يعط البحث أي نتيجة</h1>
                        @endif
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-3 sidebar margin-t-45">
            @include('templates.sidebar.help')
            @include('templates.sidebar.doctors')
        </div>

        <div class="col-md-12">
            @include('templates.partners')
        </div>
    </div>
</div>

@section('custom-js')
    <script>
        $('.main-content').find('ul').addClass('standard-list');
        $('.main-content').find('&nbsp').addClass('standard-list');
    </script>
@endsection

@include('footer')


