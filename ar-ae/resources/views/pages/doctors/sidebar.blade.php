<div class="col-md-12 page padding-l-0" id="doctor-filter">
    <div class="row yellowish-bg padding-l-0 padding-r-0 padding-t-0 padding-b-10">
        {!! Form::open(['route'=>'physician.index','method'=>'GET']) !!}
            <div class="gray-bg padding-5">
                    <input type="text" value="{{ isset($_GET['name']) ? $_GET['name'] : '' }}" class="icon-input search form-control sm" name="name">
            </div>

            <div class="col-md-12 padding-t-10">
                <h3>النوع</h3>
                <ul class="filters">
                    <li>
                        <input type="radio" id="g1" value="any" checked name="gender"/>
                        <label for="g1">الكل</label>
                    </li>
                    <li>
                        <input type="radio" id="g2" value="F" {{ isset($_GET['gender']) ? ( $_GET['gender'] == 'F' ? 'checked="checked"' : false ) : '' }} name="gender"/>
                        <label for="g2">طبيبة</label>
                    </li>
                    <li>
                        <input type="radio" id="g3" value="M" {{ isset($_GET['gender']) ? ( $_GET['gender'] == 'M' ? 'checked="checked"' : false ) : '' }} name="gender"/>
                        <label for="g3">طبيب</label>
                    </li>
                </ul>
            </div>

            <hr>

            <div class="col-md-12">
                <h3>التخصص</h3>

                @inject('departmentFetcher','App\Services\DepartmentFetcher')

                <?php $departments = $departmentFetcher->getAll(); ?>

                <ul class="filters">
                    <li>
                        <input type="radio" id="r0" checked="checked" name="specialty" value="any"/>
                        <label for="r0" class="white">الكل</label>
                    </li>
                    @foreach($departments as $data)
                        <li>
                            <input type="radio" {{ isset($_GET['specialty']) ? ( $_GET['specialty'] == $data->slug ? 'checked="checked"' : false ) : '' }} id="r{{ $data->id }}" value="{{ $data->slug }}" name="specialty"/>
                            <label for="r{{ $data->id }}" class="white">{{ $data->name }}</label>
                        </li>
                    @endforeach

                </ul>
            </div>
            <div class="col-md-12 text-right">
                <input type="submit" value="Search" class="brown-text font-13">
            </div>
        {!! Form::close() !!}
    </div>
</div>