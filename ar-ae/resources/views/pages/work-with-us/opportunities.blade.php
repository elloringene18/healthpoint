@include('header')
<div class="pull-left">
    <div class="row">
        <div class="page col-md-9 main-content">
            <h1 class="panel-title">العمل في هيلث بوينت</h1>

            @include('templates.working-for-us')

            <div class="col-md-12 pull-left">
                <div class="row">

                    @include('pages.work-with-us.nav')
                    @inject('optionFetcher','App\Services\OptionFetcher')


                    <h1 class="panel-title">فرص العمل</h1>

                    {!! str_replace("&nbsp;"," ",$optionFetcher->getBySlug('opportunities-top')) !!}

                    <div class="row padding-t-0 contents">

                        @inject('carFetcher','App\Services\CareerFetcher')


                        @foreach($carFetcher->getAll() as $career)
                            <div class="col-md-6 margin-t-20">
                                <div class="yellow-bg">
                                    <h1>{{ $career->name }}</h1>
                                    <p>Responsibilities/Description:</p>

                                    {!! str_replace("&nbsp;"," ",$career->description) !!}
                                </div>
                            </div>
                        @endforeach
                        {{--<div class="col-md-6">--}}
                            {{--<div class="yellow-bg">--}}
                                {{--<h1>Pharmacy Informatics</h1>--}}
                                {{--<p>Responsibilities/Description:</p>--}}



                                {{--<ul class="standard-list black">--}}
                                    {{--<li><p>Data base expert for Cerner (more than two years’ experience) including hospital formulary building</p></li>--}}
                                    {{--<li><p>Building Cerner application such as order sentences, IV sets, Physicians order sets (more than two years’ experience)</p></li>--}}
                                    {{--<li><p>Medication management plan and mapping in Cerner and Omnicel (more than two years’ experience)</p></li>--}}
                                    {{--<li><p>Should be a pharmacist, but not required to have HAAD license</p></li>--}}
                                {{--</ul>--}}

                                {{--<p>Also, please note that Healthpoint has been made aware of fraudulent employment offers being extended to candidates in the form of emails, mails, text messages, facsimiles and/or through fraudulent websites.</p>--}}

                                {{--<p>We take this matter seriously and are working diligently with the relevant authorities to investigate this matter.</p>--}}

                                {{--<p>While Healthpoint bears no responsibility or liability for such fraudulent activity, we are making every effort to minimize the impact of these schemes by informing candidates.</p>--}}

                            {{--</div>--}}
                        {{--</div>--}}

                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            {!! str_replace("&nbsp;"," ",$optionFetcher->getBySlug('opportunities-bottom')) !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-3 sidebar">
            @include('templates.sidebar.help')
            @include('templates.sidebar.tweets')
        </div>
    </div>
</div>

@section('custom-js')
    <script>
        $('.contents').find('ol li').css('color','#4b3328');
        $('.contents').find('ul').addClass('standard-list');
    </script>
@endsection

@include('footer')


