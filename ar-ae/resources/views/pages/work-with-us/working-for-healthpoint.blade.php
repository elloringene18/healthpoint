@include('header')
<div class="pull-left">
    <div class="row">
        <div class="page col-md-9 main-content">
            <h1 class="panel-title">العمل في هيلث بوينت</h1>

            @include('templates.working-for-us')

            <div class="col-md-12 pull-left">
                <div class="row">

                    @include('pages.work-with-us.nav')

                    <h1 class="panel-title col-xs-top-20">العمل في هيلث بوينت</h1>

                    @include('templates.employees')

                    <hr>
                    <h1>توظيف الأطباء</h1>
                    @inject('optionFetcher','App\Services\OptionFetcher')

                    {!! str_replace("&nbsp;"," ",$optionFetcher->getBySlug('working-for-us-recruit')) !!}
                    <hr>

                    {{--<div class="rtl">--}}
                        {{--<h1>    توظيف الأطباء</h1>--}}

                        {{--<p>هيلث بوينت مستشفى فائق التخصصات ويقدم تجربة شخصية وراقية لمرضانا ضمن خدمة الخمس نجوم المتاحة لدينا. خبراؤنا الطبيون ملتزمون بتقديم الرعاية وتثقيف المرضى على درجة عالية من الجودة لضمان حصول مرضانا ومجتمعنا على ما يحتاجونه لعيش حياة صحية وسعيدة.--}}

                        {{--</p>--}}


                        {{--<p>فريق الأطباء في هيلث بوينت متفانون في عملهم ولديهم الخبرة الكافية وطيف واسع من المعلومات الطبية لخدمة المجمتع. كما يحرص أطباءنا على تقديم أعلى مستويات الرعاية الصحية مدموجة مع أحدث التكنولوجيا الطبية، ونظراً لتزايد عدد المرضى فإننا نستقبل الكوادر الطبية المميزة من الأخصائيين والاستشاريين بالانضمام إلينا في هيلث بوينت.</p>--}}

                        {{--<p> للمزيد من المعلومات عن الشواغر تواصلوا معنا عن طريق البريد الالكتروني. ولطلبات التقديم يرجى إرفاق السيرة الذاتية إلى <a href="mailto:recruitment@healthpoint.ae">recruitment@healthpoint.ae</a></p>--}}
                    {{--</div>--}}


                    @include('templates.testimonials')

                </div>
            </div>
        </div>

        <div class="col-md-3 sidebar">
            @include('templates.sidebar.help')
            @include('templates.sidebar.patient-rights')
        </div>
    </div>
</div>

@section('custom-js')
    <script src="{{ asset('public/js/jquery.magnific-popup.min.js') }}"></script>

    <script>
        $('.album').magnificPopup({
            delegate: 'a', // child items selector, by clicking on it popup will open
            type: 'image'
            // other options
        });
    </script>

    <script src="{{ asset('public/js/html5lightbox.js') }}"></script>
    <script type="text/javascript">
        var html5lightbox_options = {
            watermark: " ",
            watermarklink: " "
        };
    </script>
@endsection

@include('footer')


