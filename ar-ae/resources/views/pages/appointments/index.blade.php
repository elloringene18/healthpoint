@include('header')
<div class="pull-left" style="width: 100%;">
    <div class="row">
        <div class="col-md-9 main-content page">
            <h1 class="panel-title"> طلب موعد</h1>
            <p> الرجاء تعبئة الإستمارة التالية لطلب موعد في هيلث بوينت. سيتصل بكم أحد موظفي خدمة العملاء في أقرب وقت لتأكيد الموعد.</p>

            <p>إذا كانت هناك حالة طبية طارئة، يرجى الإتصال على الرقم 998</p>

            <p class="brown-text">كل الحقول المشار إليها بـ (*) إلزامية</p>

            {!! Form::open(['class'=>'appointment','route'=>'appointment.request']) !!}
            <hr>

            <h1>معلومات مقدم الطلب</h1>

            <div class="row">
                <div class="col-md-3 col-sm-3 col-xs-12 clearfix">
                    <div class="padding-t-0 ">
                        <p class="text-left">لمن هذا الموعد؟</p>
                    </div>
                </div>
                <div class="col-md-9">
                    <div class="">
                        <input type="radio" id="r1" checked name="Who is this appointment for" value="Self"/>
                        <label for="r1">نفسك</label>
                        <br>
                        <input type="radio" id="r2" name="Who is this appointment for" value="Other"/>
                        <label for="r2">شخص آخر</label>
                    </div>
                </div>
            </div>
            <hr>


            <h1>معلومات المريض</h1>
            <p> يرجى تقديم معلومات المريض كما تظهر على الوثائق القانونية.</p>

            <div class="row">
                <div class="col-md-3 col-sm-3 col-xs-12 clearfix">
                    <div class="padding-t-0">
                        <p class="">هل حصلت سابقاً على الرعاية في هيلث بوينت؟</p>
                    </div>
                </div>
                <div class="col-md-9">
                    <div class="">
                        <input type="radio" id="r3" checked name="Have you previously received care at Healthpoint" value="Yes"/>
                        <label for="r3">نعم</label>
                        <br>
                        <input type="radio" id="r4" name="Have you previously received care at Healthpoint" value="No"/>
                        <label for="r4">لا</label>
                        <br>
                        <input type="radio" id="r5" name="Have you previously received care at Healthpoint" value="Do not know"/>
                        <label for="r5">لا أعرف</label>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-3 col-sm-3 col-xs-12 clearfix">
                    <div class="padding-t-0 ">
                        <p class=" label"> الإسم الأول:</p>
                    </div>
                </div>
                <div class="col-md-9">
                    <div class="">
                        <input type="text" required value="" name="First Name"/>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-3 col-sm-3 col-xs-12 clearfix">
                    <div class="padding-t-0 ">
                        <p class=" label">اسم العائلة:</p>
                    </div>
                </div>
                <div class="col-md-9">
                    <div class="">
                        <input type="text" required  value="" name="Surname"/>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-3 col-sm-3 col-xs-12 clearfix">
                    <div class="padding-t-0 ">
                        <p class=" label">العنوان:</p>
                    </div>
                </div>
                <div class="col-md-9">
                    <div class="">
                        <input type="text" required  value="" name="Address"/>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-3 col-sm-3 col-xs-12 clearfix">
                    <div class="padding-t-0 ">
                        <p class=" label">المدينة:</p>
                    </div>
                </div>
                <div class="col-md-9">
                    <div class="">
                        <input type="text" required  value="" name="City"/>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-3 col-sm-3 col-xs-12 clearfix">
                    <div class="padding-t-0 ">
                        <p class=" label">الإمارة:</p>
                    </div>
                </div>
                <div class="col-md-9">
                    <div class="">
                        <input type="text" required  value="" name="Emirate"/>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-3 col-sm-3 col-xs-12 clearfix">
                    <div class="padding-t-0 ">
                        <p class=" label">صندوق البريد:</p>
                    </div>
                </div>
                <div class="col-md-9">
                    <div class="">
                        <input type="text"  value="" name="P.O Box"/>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-3 col-sm-3 col-xs-12 clearfix">
                    <div class="padding-t-0 ">
                        <p class=" label">الهاتف الرئيسي:</p>
                    </div>
                </div>
                <div class="col-md-9">
                    <div class="">
                        <input type="text" required  value="" name="Primary phone" class="margin-b-10"/>
                        <br>
                        <input type="radio" id="r33" checked name="Primary phone type" value="Home"/>
                        <label for="r33">المنزل</label>
                        <br>
                        <input type="radio" id="r44" name="Primary phone type" value="Mobile"/>
                        <label for="r44">المتحرك</label>
                        <br>
                        <input type="radio" id="r55" name="Primary phone type" value="Office"/>
                        <label for="r55">العمل</label>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-3 col-sm-3 col-xs-12 clearfix">
                    <div class="padding-t-0 ">
                        <p class=" label">الهاتف الثانوي:</p>
                    </div>
                </div>
                <div class="col-md-9">
                    <div class="">
                        <input type="text" name="Secondary phone"  value="" class="margin-b-10"/>
                        <br>
                        <input type="radio" id="r23" checked name="Secondary phone type" value="Home"/>
                        <label for="r23">المنزل</label>
                        <br>
                        <input type="radio" id="r24" name="Secondary phone type" value="Mobile"/>
                        <label for="r24">المتحرك</label>
                        <br>
                        <input type="radio" id="r25" name="Secondary phone type" value="Office"/>
                        <label for="r25">العمل</label>
                    </div>
                </div>
            </div>


            <div class="row">
                <div class="col-md-3 col-sm-3 col-xs-12 clearfix">
                    <div class="padding-t-0 ">
                        <p class=" label">البريد الإلكتروني:</p>
                    </div>
                </div>
                <div class="col-md-9">
                    <div class="">
                        <input type="text" required  value="" name="Email"/>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-3 col-sm-3 col-xs-12 clearfix">
                    <div class="padding-t-0 ">
                        <p class=" label">الجنس:</p>
                    </div>
                </div>
                <div class="col-md-9">
                    <div class="">
                        <input type="radio" id="rt3" checked name="Gender" value="Male"/>
                        <label for="rt3">ذكر</label>
                        <br>
                        <input type="radio" id="rt4"  name="Gender" value="Female"/>
                        <label for="rt4">أنثى</label>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-3 col-sm-3 col-xs-12 clearfix">
                    <div class="padding-t-0 ">
                        <p class=" label">تاريخ الميلاد:</p>
                    </div>
                </div>
                <div class="col-md-9">
                    <div class="date-input">
                        <select class="selectpicker pull-left" name="Date of Birth[month]">
                            <?php
                            for($x=1;$x<13;$x++){
                                echo '<option value='.$x.'>'.$x.'</option>';
                            }
                            ?>
                        </select>
                        <select class="selectpicker pull-left" name="Date of Birth[day]">
                            <?php
                            for($x=1;$x<32;$x++){
                                echo '<option value='.$x.'>'.$x.'</option>';
                            }
                            ?>
                        </select>

                        <select class="selectpicker pull-left" name="Date of Birth[year]">
                            <?php

                            $yearNow = \Carbon\Carbon::now()->format('Y');

                            for($x=1900;$x<$yearNow;$x++){
                                echo '<option value='.$x.'>'.$x.'</option>';
                            }
                            ?>
                        </select>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-3 col-sm-3 col-xs-12 clearfix">
                    <div class="padding-t-0 ">
                        <p class=" label">اسم ولي الأمر أو الوصي:</p>
                    </div>
                </div>
                <div class="col-md-9">
                    <div class="">
                        <input type="text" required  value="" name="Parent/Guardian name"/>
                        <p class="font-10">إذا كان سن المريض تحت 16 عاماً يتطلب منه/ منها ذكر اسم أحد أولياء الأمر</p>
                    </div>
                </div>
            </div>


            <div class="row">
                <div class="col-md-3 col-sm-3 col-xs-12 clearfix">
                    <div class="padding-t-0 ">
                        <p class=" label">هل يحتاج المريض لمترجم؟</p>
                    </div>
                </div>
                <div class="col-md-9">
                    <div class=" ">
                        <input type="radio" id="rb3" checked name="Does the patient need an interpreter" value="No"/>
                        <label for="rb3">لا</label>
                        <br>
                        <input type="radio" id="rn3"  name="Does the patient need an interpreter" value="Male"/>
                        <label for="rn3">ذكر</label>
                        <br>
                        <input type="radio" id="rn4"  name="Does the patient need an interpreter" value="Female"/>
                        <label for="rn4">أنثى</label>
                    </div>
                </div>
            </div>

            <hr>


            <h1>معلومات التأمين الصحي</h1>

            <div class="row">
                <div class="col-md-3 col-sm-3 col-xs-12 clearfix">
                    <div class="padding-t-0 ">
                        <p class="">هل لدى المريض تأمين صحي؟</p>
                    </div>
                </div>
                <div class="col-md-9">
                    <div class="">
                        <input type="radio" id="rm1" checked name="Does the patient have health insurance" value="Yes"/>
                        <label for="rm1">نعم</label>
                        <br>
                        <input type="radio" id="rm2" name="Does the patient have health insurance" value="No"/>
                        <label for="rm2">لا</label>
                    </div>
                </div>

                <div class="toggle-hide">
                    <div class="col-md-3 col-sm-3 col-xs-12 clearfix">
                        <div class="padding-t-0 ">
                            <p class=" label"> اسم التأمين الصحي:</p>
                        </div>
                    </div>
                    <div class="col-md-9">
                        <div class="">
                            <input type="text" checked name="Insurance Name" />
                        </div>
                    </div>
                </div>
            </div>

            <hr>

            <h1> معلومات طبية تتعلق بحالة المريض</h1>

            <div class="row">
                <div class="col-md-3 col-sm-3 col-xs-12 clearfix">
                    <div class="padding-t-0 ">
                        <p class=""> ما هي المشكلة الطبية الأساسية أو التشخيص لطلب الموعد؟</p>
                    </div>
                </div>
                <div class="col-md-9">
                    <div class="">
                        <textarea id="r1" rows="5" required  name="What is the primary medical problem or diagnosis for the appointment request"></textarea>
                    </div>
                </div>
            </div>

            <hr>
            <p class="brown-text standard-bold font-13">هام: بعد تقديم هذا الطلب، يرجى عدم ترك هذه الصفحة حتى تظهر لك رسالة التأكيد</p>

            <div class="row">
                <div class="col-md-9 col-md-offset-3">
                    <div class="padding-t-10">
                        <input type="submit" value="ارسل الطلب">
                    </div>
                </div>
            </div>

            </form>
        </div>
        <div class="col-md-3 col-sm-3 col-xs-12 clearfix sidebar margin-t-45">
            @include('templates.sidebar.help')
            @include('templates.sidebar.tweets')
        </div>
    </div>
</div>

@section('custom-js')
    {{--<script>--}}
    {{--function showField(el) {--}}
    {{--console.log(el);--}}
    {{--$(el).closest('.row').find('.toggle-hide').show();--}}
    {{--}--}}
    {{--function hideField(el) {--}}
    {{--$(el).closest('.row').find('.toggle-hide').hide();--}}
    {{--}--}}
    {{--</script>--}}
@endsection

@include('footer')


