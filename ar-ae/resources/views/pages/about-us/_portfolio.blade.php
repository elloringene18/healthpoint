<div class="col-md-12 tab-contents {{ isset($_GET['tab']) ? ( $_GET['tab'] == 'healthcare-network' ? 'active' : '' ) : '' }}">
    <div class="row pull-left">
        <h1 class="panel-title">شبكة مبادلة للرعاية الصحية</h1>
        <h1 class="margin-t-10">انطلاقاً من المهمة الموكلة إلى "مبادلة"، والمتمثلة بالمساهمة في عملية تنويع الاقتصاد في إمارة أبوظبي</h1>
        <div class="col-md-6 padding-t-0  padding-r-0">
            <div class="padding-t-0">

                <div class="videoWrapper">
                    <video width="100%" poster="{{ asset('public/videos/catalyst.png') }}">
                        <source src="{{ $optionFetcher->getBySlug('mubadala-portfolio-catalyst-video') }}" type="video/mp4">

                        Your browser does not support the video tag.
                    </video>
                    <span class="bt"></span>
                    <a href="{{ $optionFetcher->getBySlug('mubadala-portfolio-catalyst-video') }}" class="html5lightbox cover" data-width="780" data-height="480" title='انطلاقاً من المهمة الموكلة إلى "مبادلة"، والمتمثلة بالمساهمة في عملية تنويع الاقتصاد في إمارة أبوظبي'>&nbsp;</a>
                </div>
            </div>
        </div>
        <div class="col-md-6 col-xs-left-0 col-xs-right-0">
            {!!  $optionFetcher->getBySlug('mubadala-portfolio-catalyst') !!}
        </div>

        <div class="col-md-12">
            <div class="row pull-left">
                <div class="col-md-6 gray-border-right padding-t-0 padding-r-0">
                    <h1>كليفلاند كلينك</h1>
                    {!!  $optionFetcher->getBySlug('mubadala-portfolio-cleveland') !!}
                </div>
                <div class="col-md-6 col-xs-left-0 col-xs-right-0">
                    <h1>مركز إمبيريال كوليدج لندن للسكّري</h1>
                    {!!  $optionFetcher->getBySlug('mubadala-portfolio-imperial') !!}
                </div>
            </div>
        </div>

        @inject('artFetcher','App\Services\ArticleFetcher')

        <?php $articles = $artFetcher->getByCategory(1,6); ?>

        <div class="col-md-12">
            <div class="row">
                <ul class="box-list">
                    @forelse($articles as $article)
                        <li class="col-md-6  col-sm-6">
                            <a href="{{ route('page.single',$article->slug) }}">
                                <img src="{{ $article->thumbnail }}" width="100%">
                                <h1>{{ $article->title }}</h1>
                            </a>
                            {!! \Illuminate\Support\Str::limit(strip_tags($article->content),140) !!}
                        </li>
                    @empty
                        <li class="col-md-12">None</li>
                    @endforelse
                    {{--<li class="col-md-6  col-sm-6">--}}
                    {{--<img src="{{ asset('public/images/placeholders/rectangle-x-sm.jpg') }}" width="100%">--}}
                    {{--<h1>Abu Dhabi Telemedicine Center</h1>--}}
                    {{--<p>A joint venture between Mubadala and Switzerland’s leading telemedicine provider, Medgate, to offer high quality, convenient and confidential medical consultations, over the phone.</p>--}}
                    {{--</li>--}}
                    {{--<li class="col-md-6 col-sm-6">--}}
                    {{--<img src="{{ asset('public/images/placeholders/rectangle-x-sm.jpg') }}" width="100%">--}}
                    {{--<h1>Capital Health Screening Centre</h1>--}}
                    {{--<p>A state-of-the-art medical facility that provides Health Authority – Abu Dhabi certified medical screenings for residency visas, occupational health, pensions and benefits. </p>--}}
                    {{--</li>--}}
                    {{--<li class="col-md-6 col-sm-6">--}}
                    {{--<img src="{{ asset('public/images/placeholders/rectangle-x-sm.jpg') }}" width="100%">--}}
                    {{--<h1>National Reference Laboratory</h1>--}}
                    {{--<p>Offering more than 4,000 sophisticated and specialized medical diagnostic tests to the UAE population.</p>--}}
                    {{--</li>--}}
                    {{--<li class="col-md-6 col-sm-6">--}}
                    {{--<img src="{{ asset('public/images/placeholders/rectangle-x-sm.jpg') }}" width="100%">--}}
                    {{--<h1>Tawam Molecular Imaging Centre</h1>--}}
                    {{--<p>A world-class specialist molecular imaging facility in Al Ain, clinically operated by Johns Hopkins Medicine International.</p>--}}
                    {{--</li>--}}
                </ul>
            </div>
        </div>
    </div>
</div>