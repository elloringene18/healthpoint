<div class="col-md-12">
    <div class="row">
        <ul class="border-line-list short clearfix">
            <li class="col-md-6 col-xs-6 {{ str_contains(Request::path(), 'upcoming') ? 'active' : '' }}"><a href="{{ url('events/upcoming') }}">الأحداث القادمة
                </a></li>
            <li class="col-md-6 col-xs-6 {{ str_contains(Request::path(), 'past') ? 'active' : '' }}"><a href="{{ url('events/past') }}">الأحداث الماضية
                </a></li>
        </ul>
    </div>
</div>