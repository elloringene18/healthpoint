<div class="col-md-12">
    <div class="row">
        <ul class="border-line-list short clearfix">
            <li class="col-md-4 col-xs-4 {{ str_contains(Request::path(), 'our-services') ? 'active' : '' }}"><a href="{{ url('patients/our-services') }}">خدماتنا</a></li>
            <li class="col-md-4 col-xs-4 {{ str_contains(Request::path(), 'insurance') ? 'active' : '' }}"><a href="{{ url('patients/insurance') }}">التأمين الطبي</a></li>
            <li class="col-md-4 col-xs-4 {{ str_contains(Request::path(), 'at-healthpoint') ? 'active' : '' }}"><a href="{{ url('patients/at-healthpoint') }}">ابتكارات هيلث بوينت</a></li>
        </ul>
    </div>
</div>