<?php

use Illuminate\Database\Seeder;

class PagesSeeder extends Seeder
{

    use \App\Repositories\CanCreateSlug;

    public function __construct(\App\Models\ArticleCategory $model)
    {
        $this->model = $model;
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            ['name' => 'Custom Pages']
        ];

        foreach ($data as $item){
            $item['slug'] = $this->generateSlug($item['name']);

            $this->model->create($item);
        }

    }
}
