<?php

use Illuminate\Database\Seeder;
use App\Models\Physician;
use App\Repositories\CanCreateSlug;

class PhysicianSlugSeeder extends Seeder
{
    use CanCreateSlug;

    public function __construct(Physician $physician){
        $this->model = $physician;
    }
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $physicians = Physician::get();

        foreach($physicians as $physician){
            $slug = $this->generateSlug($physician->fname . '-' . $physician->lname);

            $physician->slug = $slug;
            $physician->save();
        }

    }
}
