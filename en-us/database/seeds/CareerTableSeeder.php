<?php

use Illuminate\Database\Seeder;
use App\Models\Career;

class CareerTableSeeder extends Seeder
{

    public function __construct(Career $model)
    {
        $this->model = $model;
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'name' => 'Drug Specialist',
                'description' => "<ul><li><p>Formulary management including reviewing medications for addition and deletions, or have experience as a formulary manager</p></li><li><p>Member of Pharmacy and Therapeutic Committee (PTC) and a presenter for drug evaluations</p></li><li><p>Creating protocols and guidelines</p></li><li><p>Handling questions from physicians and pharmacists and from other sub-departments, covering a wide variety of topics including drug selection, drug interactions, pharmacology, pharmacokinetics, adverse effects, teratogenicity, lactation, product availability and cost</p></li><li><p>Processing non-formulary drug requests according to evidence-based medicine and producing a non- formulary monthly report</p></li><li><p>Participating in formulary management through processing Nil Stock report, technical evaluation and slow moving, non- moving item reports</p></li><li>- See more at: http://localhost/healthpoint/en-us/work-with-us/opportunities#sthash.IprZh1Pc.dpuf</li></ul><p>&nbsp;</p>"
            ],
            [
                'name' => 'Pharmacy Informatics',
                'description' => "<ul><li><p>Data base expert for Cerner (more than two years&rsquo; experience) including hospital formulary building</p></li><li><p>Building Cerner application such as order sentences, IV sets, Physicians order sets (more than two years&rsquo; experience)</p></li><li><p>Medication management plan and mapping in Cerner and Omnicel (more than two years&rsquo; experience)</p></li><li><p>Should be a pharmacist, but not required to have HAAD license</p></li></ul><p>&nbsp;</p><p>Also, please note that Healthpoint has been made aware of fraudulent employment offers being extended to candidates in the form of emails, mails, text messages, facsimiles and/or through fraudulent websites.</p><p>We take this matter seriously and are working diligently with the relevant authorities to investigate this matter.</p><p>While Healthpoint bears no responsibility or liability for such fraudulent activity, we are making every effort to minimize the impact of these schemes by informing candidates.</p>"
            ]
        ];

        foreach ($data as $item){
            $this->model->create($item);
        }

    }
}
