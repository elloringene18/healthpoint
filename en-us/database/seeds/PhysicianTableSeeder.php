<?php

use Illuminate\Database\Seeder;
use App\Models\Physician;

use App\Repositories\CanCreateSlug;
use App\Services\Uploaders\InsuranceThumbnailUploader;

class PhysicianTableSeeder extends Seeder
{

    public function __construct(Physician $model)
    {
        $this->model = $model;
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'name' => 'Dr. Maurice Khoury',
                'position' => '<p>MD, FRCS (Glasg)</p><p>FACS Consultant</p>',
                'description' => "<p>Specialized in advanced laparoscopic bariatric and upper gastrointestinal Surgery Course Director and instructor in more than 150 courses of basic, advanced laparoscopy and trauma management in the UAE and internationally Have many Publications in a peer reviewed journals in Bariatric surgery, Endoscopy and trauma management</p>",
                'gender' => 'M'
            ],
        ];

        foreach ($data as $item){
            $newData = $this->model->create($item);

            $newData->department()->sync(['department_id'=>2]);
        }

    }
}

