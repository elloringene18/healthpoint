@extends('emails.main')

@section('email_body')
    <br>
    <br>
    <h1>Appointment Request from {{ $data->name }}</h1>
    <br>
    @foreach($data->info as $item)
        <p><b>{{ $item->key }}:</b> {{ $item->value }}</p>
    @endforeach
@endsection