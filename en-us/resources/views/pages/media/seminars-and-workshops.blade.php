@include('header')
<div class="pull-left">
    <div class="row">
        <div class="page col-md-9 main-content">
            <h1 class="panel-title">Seminars and Workshops</h1>

            <div class="col-md-12 width-full">
                <div class="row">

                    <div class="pull-left col-md-12">
                        <div class="row">
                            <div class="row">
                                <ul class="float-list article event cols-3">
                                    @forelse($articles as $index=>$article)
                                        @if($index%3==0)
                                            <div class="clearfix">
                                        @endif
                                                <li class="col-md-4 col-sm-6 col-xs-12">
                                                <a href="{{ route('events.single',$article->slug) }}">
                                                    <img src="{{ $article->thumbnail }}" width="100%">
                                                    <h1 class="margin-t-8">{{ \Illuminate\Support\Str::limit(strip_tags($article->title),50) }}</h1>
                                                </a>
                                                <div class="row">
                                                    <div class="col-md-6"><a href="#" class="icon-text date">{{ $article->date->format('M d Y') }}</a></div>
                                                    <div class="col-md-6"><a href="#" class="icon-text time">{{ $article->time }}</a></div>
                                                    <div class="col-md-12"><a href="#" class="icon-text location">{{ $article->location }}</a></div>
                                                </div>
                                                    <p>{!! \Illuminate\Support\Str::limit(preg_replace("/&#?[a-z0-9]+;/i","",strip_tags($article->content)),140) !!}</p>
                                            </li>
                                        @if($index%3==2)
                                            </div>
                                        @endif
                                    @empty
                                        <li class="col-md-12 no-border">None</li>
                                    @endforelse

                                    {{--<li class="col-md-4 col-sm-6">--}}
                                        {{--<a href="{{ url('media/news/post') }}">--}}
                                            {{--<img src="{{ asset('public/images/placeholders/rectangle-x-sm.jpg') }}" width="100%">--}}
                                            {{--<h1 class="margin-t-8">Palpitation – CME Accredited</h1>--}}
                                        {{--</a>--}}
                                        {{--<div class="row">--}}
                                            {{--<div class="col-md-6"><a href="#" class="icon-text date">2 June 2015</a></div>--}}
                                            {{--<div class="col-md-6"><a href="#" class="icon-text time">5:30pm – 7:00pm</a></div>--}}
                                            {{--<div class="col-md-12"><a href="#" class="icon-text location">Hospital Conference Room, Ground Level</a></div>--}}
                                        {{--</div>--}}
                                        {{--<p>Dr. Alaa Ameen Ali Saeed, Consultant Interventional Cardiologist</p>--}}
                                    {{--</li>--}}
                                </ul>
                            </div>
                        </div>
                    </div>

                    <div class="pagination text-center">
                        {{ $articles->links() }}
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-3 sidebar">
            @include('templates.sidebar.help')
            @include('templates.sidebar.media-contacts')
            @include('templates.sidebar.media-pack')
            @include('templates.sidebar.tweets')
        </div>
    </div>
</div>

@section('custom-js')
    <script>
    </script>
@endsection

@include('footer')


