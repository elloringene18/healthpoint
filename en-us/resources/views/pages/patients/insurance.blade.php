
@include('header')
<div class="pull-left">
    <div class="row">
        <div class="page col-md-9 main-content">
            <h1 class="panel-title">Patient & Visitors</h1>
            @inject('optionFetcher','App\Services\OptionFetcher')
            @if($optionFetcher->getBySlug('patients-banner-image'))
                <div class="banner-box patient" style="background-image: url({{ asset('public'.$optionFetcher->getBySlug('patients-banner-image')) }})"></div>
            @else
                <div class="banner-box patient"></div>
            @endif
            <div class="col-md-12 pull-left">
                <div class="row">

                    @include('pages.patients.nav')

                    <div class="col-md-12">
                        <div class="row">

                            @inject('insFetcher','App\Services\InsuranceFetcher')

                            <?php $insurances = $insFetcher->getAll(); ?>
                            All Thiqa patients can use their insurance with us and treatment is 100% covered for insurance accepted services.<br><br>

                            <h1 class="panel-title">Insurance Partners</h1>

                            @inject('optionFetcher','App\Services\OptionFetcher')
                            {!! $optionFetcher->getBySlug('insurance-partners') !!}

                            {{--<img src="{{ asset('public/images/thiqa-banner.jpg') }}" width="100%">--}}
                            <ul class="float-list sponsor-list">

                                {{--<li><img src="{{ asset('public/images/placeholders/sponsors/ADNIC-01 Grey.jpg') }}"> </li>--}}
                                {{--<li><img src="{{ asset('public/images/placeholders/sponsors/Almadallah_logo-01Grey.jpg') }}"> </li>--}}
                                {{--<li><img src="{{ asset('public/images/placeholders/sponsors/DAMAN logo-01 Grey.jpg') }}"> </li>--}}
                                {{--<li><img src="{{ asset('public/images/placeholders/sponsors/NAS Grey.png') }}"> </li>--}}
                                {{--<li><img src="{{ asset('public/images/placeholders/sponsors/Puba Grey.png') }}"> </li>--}}
                                {{--<li><img src="{{ asset('public/images/placeholders/sponsors/saicoHealth Grey.jpg') }}"> </li>--}}
                                {{--<li><img src="{{ asset('public/images/placeholders/sponsors/thiqalogo_Grey.jpg') }}"> </li>--}}
                                {{--<li><img src="{{ asset('public/images/placeholders/sponsors/Wapmed-Logo-Grey.jpg') }}"> </li>--}}

                                @foreach($insurances as $insurance)
                                    <li><img src="{{ $insurance->thumbnail }}"> </li>
                                @endforeach

                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-3 sidebar">
            @include('templates.sidebar.help')
            @include('templates.sidebar.insurance-info')
        </div>
    </div>
</div>

@section('custom-js')
    <script>
        $('.panel-body').find('ul').addClass('standard-list');
        $('.panel-body').find('ol li').css('color','#4b3328');
    </script>
@endsection

@include('footer')


