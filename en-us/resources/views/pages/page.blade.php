@section('metas')

        <!-- Place this data between the <head> tags of your website -->
<meta name="description" content="{!! \Illuminate\Support\Str::limit(strip_tags($article->content), 150) !!}" />

<!-- Schema.org markup for Google+ -->
<meta itemprop="name" content="{{ $article->title }}">
<meta itemprop="description" content="{!! \Illuminate\Support\Str::limit(strip_tags($article->content), 150) !!}">
<meta itemprop="image" content="{{ asset( $article->thumbnail) }}">

<!-- Twitter Card data -->
<meta name="twitter:card" content="summary_large_image">
<meta name="twitter:site" content="www..com">
<meta name="twitter:title" content="{{ $article->title }}">
<meta name="twitter:description" content="{!!\Illuminate\Support\Str::limit(strip_tags($article->content), 150) !!}">
<meta name="twitter:creator" content="Middle East Doctor">
<!-- Twitter summary card with large image must be at least 280x150px -->
<meta name="twitter:image:src" content="{{ asset( $article->thumbnail) }}">

<!-- Open Graph data -->
<meta property="og:title" content="{{ $article->title }}" />
<meta property="og:type" content="article" />
<meta property="og:url" content="{{ Request::fullUrl()}}" />
<meta property="og:image" content="{{ asset( $article->thumbnail) }}" />
<meta property="og:description" content="{!!\Illuminate\Support\Str::limit(strip_tags($article->content), 150) !!}" />
<meta property="og:site_name" content="www..com" />
<meta property="article:published_time" content="{{ $article->created_at }}" />
<meta property="article:modified_time" content="{{ $article->updated_at }}" />
<meta property="article:section" content="Article Section" />
<meta property="article:tag" content="Article Tag" />
<meta property="fb:admins" content="100000370643856" />

<style>
    .stButton {
        height: 24px !important;
        position: absolute !important;
        left: 0 !important;
        opacity: 0 !important;
        width: 100% !important;
    }
    .stButton .stLarge {
        width: 100% !important;
    }
    .stButton:hover {
        height: 24px !important;
        width: 21px !important;
        opacity: 0 !important;
    }
</style>
@stop


@include('header')

<div class="pull-left">
    <div class="row">
        <div class="page col-md-9 main-content blog pull-left">
            <div class="col-md-12">
                <div class="row">
                    @if($article)
                        <h1 class="panel-title">{{ $article->title }}</h1>
                        <img src="{{ $article->thumbnailHigh }}" class="margin-t-5" width="100%">

                        <div class="col-md-12">
                            <div class="row">
                                {!! $article->content !!}

                                @if($article->slug=='healthpoints-manchester-city-football-club-mascot-competition')
                                    <script async src="https://d36hc0p18k1aoc.cloudfront.net/pages/a5b5e5.js"></script>
                                    <div class="tintup" data-id="healthpointmcfc" data-columns="" data-expand="true"    data-infinitescroll="true" data-personalization-id="835491" style="height:500px;width:100%;"></div>
                                @endif

                                <a class="pull-right" href="{{ route('home') }}">Back</a>
                            </div>
                        </div>
                    @else
                        <h1 class="alert-danger alert">Unknown Article</h1>
                    @endif
                </div>
            </div>
        </div>

        <div class="col-md-3 sidebar">
            @include('templates.sidebar.help')
            @include('templates.sidebar.more')
        </div>
    </div>
</div>

@section('custom-js')
    <script>
        $('.main-content').find('ul').addClass('standard-list');
        $('.main-content').find('ol li').css('color','#4b3328');
    </script>
@endsection

@include('footer')


