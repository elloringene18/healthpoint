<div class="col-md-12 pull-left width-full">
    <div class="row">
        <ul class="border-line-list short clearfix">
            <li class="col-md-4 col-xs-4 {{ !str_contains(Request::path(), '/') ? 'active' : '' }}"><a href="{{ url('work-with-us') }}">Working for Healthpoint</a></li>
            <li class="col-md-4 col-xs-4 {{ str_contains(Request::path(), 'emiratization') ? 'active' : '' }}"><a href="{{ url('work-with-us/emiratization') }}">Emiratization</a></li>
            <li class="col-md-4 col-xs-4 {{ str_contains(Request::path(), 'careers') ? 'active' : '' }}"><a href="{{ url('work-with-us/careers') }}">Careers</a></li>
        </ul>
    </div>
</div>