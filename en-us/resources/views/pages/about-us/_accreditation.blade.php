<div class="col-md-12 tab-contents {{ isset($_GET['tab']) ? ( $_GET['tab'] == 'accreditation' ? 'active' : '' ) : '' }}">
<div class="row">
        <h1 class="panel-title">Accreditation</h1>
        <div class="col-md-12 margin-b-10">
            <div class="row">
                {{-- {!! $optionFetcher->getBySlug('about-us') !!} --}}
               <img src="{{ asset('public/images/JCI-Logo.png') }}">
                JCI is a not-for-profit organization dedicated to improving the quality and safety of patient care around the world. Healthpoint received the Gold Seal of Approval® from the Joint Commission International (JCI) which accredited our fully integrated, super-specialty hospital. Meeting JCI’s demanding standards means that we deliver the highest quality and patient- safe care possible and reaffirms our commitment to world-class healthcare to serve the UAE community.
            </div>
        </div>
    </div>
</div>