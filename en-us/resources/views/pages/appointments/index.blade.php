@include('header')
<div class="pull-left">
    <div class="row">
        <div class="col-md-9 main-content page">
            <h1 class="panel-title">Request an Appointment</h1>
            <p>Complete the form below to request an appointment at Healthpoint. An appointment representative will contact you to book
                your appointment. </p>

            <p>If you have a medical emergency, call 998.</p>

            <p class="brown-text">All fields are required unless marked optional.</p>

            {!! Form::open(['class'=>'appointment','route'=>'appointment.request']) !!}
            <hr>

            <h1>Requester Information</h1>

            <div class="row">
                <div class="col-md-3 col-sm-3 col-xs-12 clearfix">
                    <div class="padding-t-0 ">
                        <p class="text-left">Who is this appointment for?</p>
                    </div>
                </div>
                <div class="col-md-9">
                    <div class="">
                        <input type="radio" id="r1" checked name="Who is this appointment for" value="Self"/>
                        <label for="r1">Self</label>
                        <br>
                        <input type="radio" id="r2" name="Who is this appointment for" value="Other"/>
                        <label for="r2">Other</label>
                    </div>
                </div>
            </div>
            <hr>


            <h1>Patient Information</h1>
            <p>Please provide patient information as it appears on legal documents.</p>

            <div class="row">
                <div class="col-md-3 col-sm-3 col-xs-12 clearfix">
                    <div class="padding-t-0">
                        <p class="">Have you previously received care at Healthpoint?</p>
                    </div>
                </div>
                <div class="col-md-9">
                    <div class="">
                        <input type="radio" id="r3" checked name="Have you previously received care at Healthpoint" value="Yes"/>
                        <label for="r3">Yes</label>
                        <br>
                        <input type="radio" id="r4" name="Have you previously received care at Healthpoint" value="No"/>
                        <label for="r4">No</label>
                        <br>
                        <input type="radio" id="r5" name="Have you previously received care at Healthpoint" value="Do not know"/>
                        <label for="r5">Don't know</label>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-3 col-sm-3 col-xs-12 clearfix">
                    <div class="padding-t-0 ">
                        <p class=" label">First Name:</p>
                    </div>
                </div>
                <div class="col-md-9">
                    <div class="">
                        <input type="text" required value="" name="First Name"/>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-3 col-sm-3 col-xs-12 clearfix">
                    <div class="padding-t-0 ">
                        <p class=" label">Surname:</p>
                    </div>
                </div>
                <div class="col-md-9">
                    <div class="">
                        <input type="text" required  value="" name="Surname"/>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-3 col-sm-3 col-xs-12 clearfix">
                    <div class="padding-t-0 ">
                        <p class=" label">Address:</p>
                    </div>
                </div>
                <div class="col-md-9">
                    <div class="">
                        <input type="text" required  value="" name="Address"/>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-3 col-sm-3 col-xs-12 clearfix">
                    <div class="padding-t-0 ">
                        <p class=" label">City:</p>
                    </div>
                </div>
                <div class="col-md-9">
                    <div class="">
                        <input type="text" required  value="" name="City"/>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-3 col-sm-3 col-xs-12 clearfix">
                    <div class="padding-t-0 ">
                        <p class=" label">Emirate:</p>
                    </div>
                </div>
                <div class="col-md-9">
                    <div class="">
                        <input type="text" required  value="" name="Emirate"/>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-3 col-sm-3 col-xs-12 clearfix">
                    <div class="padding-t-0 ">
                        <p class=" label">P.O Box:</p>
                    </div>
                </div>
                <div class="col-md-9">
                    <div class="">
                        <input type="text"  value="" name="P.O Box"/>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-3 col-sm-3 col-xs-12 clearfix">
                    <div class="padding-t-0 ">
                        <p class=" label">Primary phone:</p>
                    </div>
                </div>
                <div class="col-md-9">
                    <div class="">
                        <input type="text" required  value="" name="Primary phone" class="margin-b-10"/>
                        <br>
                        <input type="radio" id="r33" checked name="Primary phone type" value="Home"/>
                        <label for="r33">Home</label>
                        <br>
                        <input type="radio" id="r44" name="Primary phone type" value="Mobile"/>
                        <label for="r44">Mobile</label>
                        <br>
                        <input type="radio" id="r55" name="Primary phone type" value="Office"/>
                        <label for="r55">Office</label>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-3 col-sm-3 col-xs-12 clearfix">
                    <div class="padding-t-0 ">
                        <p class=" label">Secondary phone:</p>
                    </div>
                </div>
                <div class="col-md-9">
                    <div class="">
                        <input type="text" name="Secondary phone"  value="" class="margin-b-10"/>
                        <br>
                        <input type="radio" id="r23" checked name="Secondary phone type" value="Home"/>
                        <label for="r23">Home</label>
                        <br>
                        <input type="radio" id="r24" name="Secondary phone type" value="Mobile"/>
                        <label for="r24">Mobile</label>
                        <br>
                        <input type="radio" id="r25" name="Secondary phone type" value="Office"/>
                        <label for="r25">Office</label>
                    </div>
                </div>
            </div>


            <div class="row">
                <div class="col-md-3 col-sm-3 col-xs-12 clearfix">
                    <div class="padding-t-0 ">
                        <p class=" label">Email:</p>
                    </div>
                </div>
                <div class="col-md-9">
                    <div class="">
                        <input type="text" required  value="" name="Email"/>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-3 col-sm-3 col-xs-12 clearfix">
                    <div class="padding-t-0 ">
                        <p class=" label">Gender:</p>
                    </div>
                </div>
                <div class="col-md-9">
                    <div class="">
                        <input type="radio" id="rt3" checked name="Gender" value="Male"/>
                        <label for="rt3">Male</label>
                        <br>
                        <input type="radio" id="rt4"  name="Gender" value="Female"/>
                        <label for="rt4">Female</label>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-3 col-sm-3 col-xs-12 clearfix">
                    <div class="padding-t-0 ">
                        <p class=" label">Date of Birth:</p>
                    </div>
                </div>
                <div class="col-md-9">
                    <div class="date-input">
                        <select class="selectpicker pull-left" name="Date of Birth[month]">
                            <?php
                                for($x=1;$x<13;$x++){
                                    echo '<option value='.$x.'>'.$x.'</option>';
                                }
                            ?>
                        </select>
                        <select class="selectpicker pull-left" name="Date of Birth[day]">
                            <?php
                            for($x=1;$x<32;$x++){
                                echo '<option value='.$x.'>'.$x.'</option>';
                            }
                            ?>
                        </select>
                        <select class="selectpicker pull-left" name="Date of Birth[year]">
                            <?php

                            $yearNow = \Carbon\Carbon::now()->format('Y');

                            for($x=1900;$x<$yearNow;$x++){
                                echo '<option value='.$x.'>'.$x.'</option>';
                            }
                            ?>
                        </select>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-3 col-sm-3 col-xs-12 clearfix">
                    <div class="padding-t-0 ">
                        <p class=" label">Parent/Guardian name:</p>
                    </div>
                </div>
                <div class="col-md-9">
                    <div class="">
                        <input type="text" required  value="" name="Parent/Guardian name"/>
                        <p class="font-8">The name of a parent is required if <br>the patient is under the age of 16.</p>
                    </div>
                </div>
            </div>


            <div class="row">
                <div class="col-md-3 col-sm-3 col-xs-12 clearfix">
                    <div class="padding-t-0 ">
                        <p class=" label">Does the patient need <br>an interpreter?</p>
                    </div>
                </div>
                <div class="col-md-9">
                    <div class=" ">
                        <input type="radio" id="rb3" checked name="Does the patient need an interpreter" value="No"/>
                        <label for="rb3">No</label>
                        <br>
                        <input type="radio" id="rn3"  name="Does the patient need an interpreter" value="Male"/>
                        <label for="rn3">Male</label>
                        <br>
                        <input type="radio" id="rn4"  name="Does the patient need an interpreter" value="Female"/>
                        <label for="rn4">Female</label>
                    </div>
                </div>
            </div>

            <hr>


            <h1>Patient Insurance Information</h1>

            <div class="row">
                <div class="col-md-3 col-sm-3 col-xs-12 clearfix">
                    <div class="padding-t-0 ">
                        <p class="">Does the patient have health insurance?</p>
                    </div>
                </div>
                <div class="col-md-9">
                    <div class="">
                        <input type="radio" id="rm1" checked name="Does the patient have health insurance" value="Yes"/>
                        <label for="rm1">Yes</label>
                        <br>
                        <input type="radio" id="rm2" name="Does the patient have health insurance" value="No"/>
                        <label for="rm2">No</label>
                    </div>
                </div>

                <div class="toggle-hide">
                    <div class="col-md-3 col-sm-3 col-xs-12 clearfix">
                        <div class="padding-t-0 ">
                            <p class=" label">Insurance Name:</p>
                        </div>
                    </div>
                    <div class="col-md-9">
                        <div class="">
                            <input type="text" checked name="Insurance Name" />
                        </div>
                    </div>
                </div>
            </div>

            <hr>

            <h1>Medical Concern</h1>

            <div class="row">
                <div class="col-md-3 col-sm-3 col-xs-12 clearfix">
                    <div class="padding-t-0 ">
                        <p class="">What is the primary medical
                            problem or diagnosis for the
                            appointment request?</p>
                    </div>
                </div>
                <div class="col-md-9">
                    <div class="">
                        <textarea id="r1" rows="5" required  name="What is the primary medical problem or diagnosis for the appointment request"></textarea>
                    </div>
                </div>
            </div>

            <hr>
            <p class="brown-text standard-bold font-13">Important: After submission, please do not leave this form until you see the confirmation message.</p>

            <div class="row">
                <div class="col-md-9 col-md-offset-3">
                    <div class="padding-t-10">
                        <input type="submit" value="Send Request">
                    </div>
                </div>
            </div>

            </form>
        </div>
        <div class="col-md-3 col-sm-3 col-xs-12 clearfix sidebar margin-t-45">
            @include('templates.sidebar.help')
            @include('templates.sidebar.tweets')
        </div>
    </div>
</div>

@section('custom-js')
    {{--<script>--}}
        {{--function showField(el) {--}}
            {{--console.log(el);--}}
            {{--$(el).closest('.row').find('.toggle-hide').show();--}}
        {{--}--}}
        {{--function hideField(el) {--}}
            {{--$(el).closest('.row').find('.toggle-hide').hide();--}}
        {{--}--}}
    {{--</script>--}}
@endsection

@include('footer')


