@inject('optionFetcher','App\Services\OptionFetcher')

<?php
$clinicHours = $optionFetcher->getBySlug('clinic-hours');
$location = $optionFetcher->getBySlug('location');
$contact = $optionFetcher->getBySlug('contact');
?>

<h1 class="panel-title">Hospital <br>Information</h1>

<h3 class="standard-bold">General Outpatient</h3>
<h3 class="standard-bold">Clinic Hours:</h3>
{!! $clinicHours ? $clinicHours : 'N/A' !!}

<hr class="brown">

<h3 class="standard-bold">Location</h3>
{!! $location ? $location : 'N/A' !!}

<hr class="brown">

<h3 class="standard-bold">Contact</h3>
{!! $contact ? $contact : 'N/A' !!}

