@extends('admin.master')

@section('custom-css')
    <style>
        .field {
            position: relative;
            margin-bottom: 5px;
        }
    </style>
@endsection

@section('content')
        <!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Physicians
        <small>Control panel</small>
        <a class="pull-right" style="font-size: 16px;" href="{{ route('admin.physicians.create') }}">+Add Physician</a>
    </h1>
</section>

<!-- Main content -->
<section class="content">
    <div class="box box-info">
        <div class="box-header">
            <div class="col-md-12">
            @if(Session::has('success'))
                <div class="alert alert-success">
                    {{ Session::get('success') }}
                </div>
            @endif

            <h3 class="box-title">{{ isset($data) ? 'Edit' : 'Add' }} Physician</h3>

            <!-- /. tools -->
            </div>
        </div>
        <!-- /.box-header -->

        <div class="row">
            <div class="col-md-12">
                <div class="box-body pad">
                    {!! Form::open(['route'=> isset($data) ? 'admin.physicians.update' : 'admin.physicians.store','files'=>true]) !!}

                    @if(isset($data))
                        <div class="col-md-12">
                            <div class="col-md-8">
                            <img src="{{ $data->thumbnail }}" height="100">
                            <br>
                            Remove Thumbnail &nbsp;<input type="checkbox" name="remove_thumb">
                            <br>
                            <br>
                            </div>

                            <div class="col-md-4">
                                @if($data->isFeatured)
                                    <div class="alert alert-success pull-right"><a href="{{ route('admin.physicians.unfeature',$data->id) }}">Remove as Featured</a></div>
                                @else
                                    <div class="alert alert-success pull-right"><a href="{{ route('admin.physicians.feature',$data->id) }}">Feature this Physician</a></div>
                                @endif

                                <br>
                                <div class="alert alert-warning pull-right">
                                    <a href="{{ route('admin.physicians.delete',$data->id) }}" onclick="return window.confirm('Are you sure you want to delete this?');">Delete Physician</a>
                                </div>
                            </div>
                        </div>
                    @endif

                        @if(isset($data))
                            <input type="hidden" name="id" value="{{ $data->id }}">
                        @endif
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>First Name</label>
                                <input type="text" name="fname" required class="form-control" value="{{ isset($data) ? $data->fname : '' }}">
                            </div>
                            <div class="form-group">
                                <label>Position</label>
                                <input type="text" name="position" required value="{{ isset($data) ? $data->position : '' }}" class="form-control">
                            </div>
                            <div class="form-group">
                                <label>Gender</label>
                                <?php
                                    $gender = [
                                        'M' => 'Male',
                                        'F' => 'Female',
                                    ]
                                ?>
                                {!! Form::select('gender',$gender,isset($data) ? $data->gender : 'M',['class'=>'form-control']) !!}
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Last Name</label>
                                <input type="text" name="lname" required class="form-control" value="{{ isset($data) ? $data->lname : '' }}">
                            </div>
                            <div class="form-group">
                                <label>Specialty</label>

                                @inject('fetcher','App\Services\DepartmentFetcher')
                                <?php $categories = $fetcher->getListId(); ?>
                                {!! Form::select('department_id',$categories, isset($data) ? $data->departmentId : 0 ,['class'=>'form-control']) !!}
                            </div>
                            <div class="form-group">
                                <label>Contact Number</label>
                                <input type="text" name="contact-number" value="{{ isset($data) ? $data->contactNumber : '' }}" class="form-control">
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Photo</label>
                                <input type="file" class="form-control"  value="" name="thumbnail">
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Professional Summary</label>
                                <textarea class="editor" name="description" rows="10" cols="80">{{ isset($data) ? $data->description : '' }}</textarea>
                            </div>
                        </div>

                        @foreach($metaTypes as $type)
                            @if($type->slug != 'contact-number')


                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>{{ $type->name }}</label>
                                    <textarea class="editor" name="meta[{{ $type->slug }}]" rows="10" cols="80">{!! isset($data) ? ( isset($data['meta'][$type->slug][0]) ? $data['meta'][$type->slug][0]['value'] : false ) : ''  !!}</textarea>
                                </div>
                            </div>
                            @endif
                        @endforeach

                        <div class="col-md-12">
                            <p>&nbsp;</p>
                            <div class="form-group">
                                <input type="submit" class="form-control">
                            </div>
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</section>
<!-- /.content -->

@endsection


@section('custom-js')
    <script>
        $('.add-field').on('click',function(e){
            e.preventDefault();
            type = $(this).attr('data-type');

            var newInput = '<div class="field">'+
                                '<input type="text" name="meta['+type+'][]" value="" class="form-control">'+
                                '<button class="remove-field pull-right">x</button>'+
                            '</div>';

            $(newInput).insertBefore(this);

            attachRemoveEvent();
        });

        function attachRemoveEvent(){
            $('.remove-field').on('click',function(e){
                e.preventDefault();
                $(this).closest('.field').remove();
            });
        }
        attachRemoveEvent();
    </script>
@endsection