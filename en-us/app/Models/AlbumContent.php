<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AlbumContent extends Model
{
    //
    protected $fillable = ['album_id','name'];

    /**
     * An article has uploads.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function uploads()
    {
        return $this->morphMany('App\Models\Upload', 'uploadable');
    }

    /**
     * get Thumbnail Attrib
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function getThumbnailAttribute()
    {
        $thumb = $this->uploads()->where('template', 'thumb')->first();

        return $thumb ? asset('public'.$thumb->url ) : 'http://placehold.it/320x240';
    }

    /**
     * get Thumbnail Attrib
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function getHighResAttribute()
    {
        $thumb = $this->uploads()->where('template', 'high')->first();

        return $thumb ? asset('public'.$thumb->url ) : 'http://placehold.it/320x240';
    }
}
