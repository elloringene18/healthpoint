<?php

namespace App\Models;

use App\Repositories\Traits\Fetcher;
use Illuminate\Database\Eloquent\Model;

use App\Models\PhysicianMetaData;
use Nicolaslopezj\Searchable\SearchableTrait;

class Physician extends Model
{
    use SearchableTrait;

    /**
     * Searchable rules.
     *
     * @var array
     */
    protected $searchable = [
        'columns' => [
            'fname' => 10,
            'lname' => 10
        ]
    ];

    protected $fillable = ['fname','lname','slug','position','description','gender','department_id'];

    /**
     * A user has a profile.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function uploads()
    {
        return $this->morphOne('App\Models\Upload', 'uploadable');
    }

    /**
     * get Thumbnail Attrib
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function getThumbnailAttribute()
    {
        $thumb = $this->uploads()->where('template', 'thumb')->first();

        return $thumb ? asset('public').$thumb->url : 'http://placehold.it/240x280';
    }

    /**
     * get Thumbnail Attrib
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function getThumbnailFullAttribute()
    {
        $thumb = $this->uploads()->where('template', 'full')->first();

        return $thumb ? asset('public').$thumb->url : 'http://placehold.it/240x280';
    }


    public function getIsFeaturedAttribute()
    {
        $option = Option::where('slug','featured-physician')->where('value',$this->id)->first();

        return $option ? true : false;
    }


    public function getNameAttribute()
    {
        return $this->fname . ' ' . $this->lname;
    }

    public function department(){
        return $this->belongsToMany('App\Models\Department','physician_departments');
    }

    public function meta(){
        return $this->belongsToMany('App\Models\PhysicianMetaType','physician_meta_data','physician_id','meta_type_id');
    }

    public function getMeta($type){
        $meta = PhysicianMetaType::where('slug',$type)->first();
        return PhysicianMetaData::where('physician_id',$this->id)->where('meta_type_id',$meta->id)->get();
    }

    public function getDepartmentIdAttribute(){
        return $this->department()->first() ? $this->department()->first()->id : null;
    }

    public function getContactNumberAttribute(){
        $meta = PhysicianMetaType::where('slug','contact-number')->first();

        if($meta){
            $data = PhysicianMetaData::where('physician_id',$this->id)->where('meta_type_id',$meta->id)->first();

            if($data)
                return $data->value;
        }

        return null;
    }

    public function getSpecialtyAttribute(){

        if(!count($this->department))
            return null;

        $data = $this->department->first()->name;

        return $data ? $data : null;
    }
}
