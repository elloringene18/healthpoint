<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Nicolaslopezj\Searchable\SearchableTrait;

class Article extends Model
{
    use SearchableTrait;

    /**
     * Searchable rules.
     *
     * @var array
     */
    protected $searchable = [
        'columns' => [
            'title' => 10,
            'content' => 10
        ]
    ];

    protected $fillable = ['title', 'slug', 'subtitle', 'date_posted', 'content', 'category_id','external_link','created_at'];

    public $dates = ['date_posted','created_at'];

    /**
     * An article has uploads.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function uploads()
    {
        return $this->morphOne('App\Models\Upload', 'uploadable');
    }

    /**
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function category()
    {
        return $this->hasOne('App\Models\ArticleCategory','id','category_id');
    }

    /**
     * get Thumbnail Attrib
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function getThumbnailSquareAttribute()
    {
        $thumb = $this->uploads()->where('template', 'square')->first();

        return $thumb ? asset('public'.$thumb->url ) : 'http://placehold.it/240x240';
    }

    /**
     * get Thumbnail Attrib
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function getThumbnailLongAttribute()
    {
        $thumb = $this->uploads()->where('template', 'long')->first();

        return $thumb ? asset('public'.$thumb->url ) : 'http://placehold.it/240x200';
    }

    /**
     * get Thumbnail Attrib
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function getThumbnailAttribute()
    {
        $thumb = $this->uploads()->where('template', 'thumb')->first();
        return $thumb ? asset('public'.$thumb->url ) : 'http://placehold.it/320x240';
    }

    /**
     * get Thumbnail Attrib
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function getThumbnailOriginalAttribute()
    {
        $thumb = $this->uploads()->where('template', 'original')->first();
        return $thumb ? asset('public'.$thumb->url ) : 'http://placehold.it/320x240';
    }

    /**
     * get Thumbnail Attrib
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function getThumbnailHighAttribute()
    {
        $thumb = $this->uploads()->where('template', 'high')->first();
        return $thumb ? asset('public'.$thumb->url ) : 'http://placehold.it/320x240';
    }

    /**
     * get Thumbnail Attrib
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function getCategoryNameAttribute()
    {
        $this->load('category');
        return isset($this->category->name) ? $this->category->name : 'N/A';
    }

}
