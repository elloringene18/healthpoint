<?php
namespace App\Repositories;

use App\Models\Article;
use App\Models\Option;
use App\Services\Uploaders\ArticleThumbnailUploader;
use App\Services\Uploaders\BannerUploader;
use Carbon\Carbon;
use GuzzleHttp\Psr7\Request;

class OptionManager {
    use CanCreateSlug;

    public function __construct(Option $model, BannerUploader $uploader)
    {
        $this->model = $model;
        $this->uploader = $uploader;
    }

    public function getByCategory($cat_id,$limit){
        $data = $this->model->where('category_id',$cat_id)->limit($limit)->get();
        return $data;
    }

    public function store($request){

        $input = $request->except('_token','survey-emails');
        $files = $request->file();

        Option::where('slug','survey-emails')->delete();

        if($request->has('survey-emails')){

            $emails = explode(',',$request->input('survey-emails'));

            foreach ($emails as $email){

                if (!filter_var($email, FILTER_VALIDATE_EMAIL) === false) {
                    Option::create([
                        'name' => 'Survey Emails',
                        'slug' => 'survey-emails',
                        'value' => trim(strip_tags($email))
                    ]);
                }
            }
        }


        foreach($input as $key=>$option){
            Option::where('slug',$key)->delete();

            $new = [
                'name' => ucwords(str_replace('-',' ',$key)),
                'slug' => $key,
                'value' => $option
                ];

            $this->model->create($new);

        }

        foreach($files as $key=>$file){
            $option = Option::where('slug',$key)->first();

            $photo = ($file != null ? $this->uploader->upload($file) : false);

            if($photo){
                $option->uploads()->delete();
                $option->uploads()->createMany($photo);
                $option->value = $option->banner;
            }

            $option->save();
        }

        return true;
    }

    public function update($request){

        $input = $request->except('_token');
        $files = $request->file();

        foreach($input as $key=>$option){
            $current = Option::where('slug',$key)->first();

            if($current)
                $current->update(['value'=>$option]);
            else {
                $new = [
                    'name' => ucwords(str_replace('-',' ',$key)),
                    'slug' => $key,
                    'value' => $option
                ];

                $this->model->create($new);
            }

        }

        foreach($files as $key=>$file){
            $option = Option::where('slug',$key)->first();

            $photo = ($file != null ? $this->uploader->upload($file) : false);

            if($photo){
                $option->uploads()->delete();
                $option->uploads()->createMany($photo);
                $option->value = $option->banner;
            }

            $option->save();
        }

        return true;
    }
}