<?php
namespace App\Repositories;

use App\Models\Album;
use App\Models\Article;
use App\Models\Insurance;
use App\Services\Uploaders\AlbumPhotosUploader;
use App\Services\Uploaders\AlbumThumbnailUploader;
use App\Services\Uploaders\InsuranceThumbnailUploader;
use FFMpeg\Coordinate\TimeCode;
use FFMpeg\FFMpeg;
use GuzzleHttp\Psr7\Request;


use Symfony\Component\Process\Exception\ProcessFailedException;
use Symfony\Component\Process\Process;

use Collective\Remote\RemoteFacade as SSH;
class GalleryManager {
    use CanCreateSlug;

    public function __construct(Album $model, AlbumThumbnailUploader $uploader, AlbumPhotosUploader $photosUploader)
    {
        $this->model = $model;
        $this->uploader = $uploader;
        $this->photos = $photosUploader;
    }

    public function getByCategory($cat_id,$limit){
        $data = $this->model->where('category_id',$cat_id)->limit($limit)->get();
        return $data;
    }

    public function store($request){
        $input = $request->input();
        $thumb = $request->file('thumbnail');
        $photos = $request->file('photos');

        $input['slug'] = $this->generateSlug($input['title']);
        $input['created_at'] = $input['created_at'] ? strtotime($input['created_at']) : Carbon::now();
        $data = $this->model->create($input);

        if($data){
            $nthumb = ($thumb != null ? $this->uploader->upload($thumb) : false);

            if($nthumb)
                $data->uploads()->createMany($nthumb);

        }


        if($data){

            foreach($photos as $photo){

                $content = $data->contents()->create(['name'=>$photo->getClientOriginalName()]);


                $nphotos = ($photo != null ? $this->photos->upload($photo) : false);

                 // /Applications/XAMPP/xamppfiles/htdocs/healthpoint/en-us/public/ffmpeg/ffmpeg -ss 2 -i /Applications/XAMPP/xamppfiles/htdocs/healthpoint/en-us/public//uploads/galleries/photos//c9cc896788a80e8df571e22b92d8a86c.mp4 -t 1 -s 480x300 -f image2 imagefilessss.jpg
                SSH::run('/Applications/XAMPP/xamppfiles/htdocs/healthpoint/en-us/public/ffmpeg/ffmpeg -ss 2 -i /Applications/XAMPP/xamppfiles/htdocs/healthpoint/en-us/public//uploads/galleries/photos//c9cc896788a80e8df571e22b92d8a86c.mp4 -t 1 -s 480x300 -f image2 iamgo.jpg');

                // executes after the command finishes
                dd('done');

                $content->uploads()->createMany($nphotos);

            }

        }


        return $data;
    }

    public function update($request){
        $input = $request->except('id');
        $thumb = $request->file('thumbnail');
        $photos = $request->file('photos');

        $data = $this->model->where('id',$request->input('id'))->first();

        if($data){

            if($input['title'] != $data->title)
                $input['slug'] = $this->generateSlug($input['title']);

            $input['created_at'] = $input['created_at'] ? strtotime($input['created_at']) : Carbon::now();

            $data->update($input);

            if(isset($input['remove_thumb']))
                $data->uploads()->delete();


            if($data){
                $nthumb = ($thumb != null ? $this->uploader->upload($thumb) : false);

                if($nthumb)
                    $data->uploads()->createMany($nthumb);

            }

            if($data){

                foreach($photos as $photo){
                    if($photo){
                        $content = $data->contents()->create(['name'=>$photo->getClientOriginalName()]);

                        $nphotos = ($photo != null ? $this->photos->upload($photo) : false);

                        $content->uploads()->createMany($nphotos);
                    }
                }

            }
        }

        return $data;
    }
}