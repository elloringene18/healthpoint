<?php
namespace App\Repositories;

use App\Models\Article;
use App\Models\Career;
use App\Models\Insurance;
use App\Services\Uploaders\InsuranceThumbnailUploader;
use GuzzleHttp\Psr7\Request;

class CareerManager {
    use CanCreateSlug;

    public function __construct(Career $model)
    {
        $this->model = $model;
    }

    public function store($request){
        $input = $request->input();

        $data = $this->model->create($input);

        return $data;
    }

    public function update($request){
        $input = $request->except('id');

        $data = $this->model->where('id',$request->input('id'))->first();

        if($data)
            $data->update($input);

        return $data;
    }
}