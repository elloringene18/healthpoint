<?php

namespace App\Http\Controllers;

use App\Models\Album;
use App\Models\AppointmentRequest;
use App\Models\Career;
use App\Models\Department;
use App\Models\Event;
use App\Models\Insurance;
use App\Models\Management;
use App\Models\Option;
use App\Models\Physician;
use App\Models\PhysicianMetaType;
use App\Models\Video;
use App\Models\Tweet;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Models\Article;

class AdminController extends Controller
{

    public function index()
    {
        return view('admin.index');
    }

    public function login()
    {
        return view('admin.login');
    }

    public function articles()
    {
        $articles = Article::orderBy('created_at','DESC')->get();

        return view('admin.articles.index',compact('articles'));
    }

    public function physicians()
    {
        $data = Physician::orderBy('created_at','DESC')->get();

        return view('admin.physicians.index',compact('data'));
    }

    public function departments()
    {
        $data = Department::orderBy('created_at','DESC')->get();

        return view('admin.departments.index',compact('data'));
    }

    public function insurances()
    {
        $data = Insurance::orderBy('created_at','DESC')->get();

        return view('admin.insurances.index',compact('data'));
    }

    public function careers()
    {
        $data = Career::orderBy('created_at','DESC')->get();

        return view('admin.careers.index',compact('data'));
    }

    public function galleries()
    {
        $data = Album::orderBy('created_at','DESC')->get();

        return view('admin.galleries.index',compact('data'));
    }

    public function management()
    {
        $data = Management::orderBy('created_at','DESC')->get();

        return view('admin.management.index',compact('data'));
    }

    public function events()
    {
        $data = Event::orderBy('created_at','DESC')->get();

        return view('admin.events.index',compact('data'));
    }

    public function videos()
    {
        $data = Video::orderBy('created_at','DESC')->get();

        return view('admin.videos.index',compact('data'));
    }

    public function appointments()
    {
        $data = AppointmentRequest::orderBy('created_at','DESC')->get();

        return view('admin.appointments.index',compact('data'));
    }

    public function options()
    {
        $data = Option::orderBy('created_at','DESC')->get();

        return view('admin.settings.general',compact('data'));
    }

    public function tweets()
    {
        $data = Tweet::orderBy('created_at','DESC')->get();

        return view('admin.tweets.index',compact('data'));
    }
}
