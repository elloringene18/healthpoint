<?php

namespace App\Http\Controllers;

use App\Models\Album;
use App\Models\AlbumContent;
use App\Models\Article;
use App\Models\Event;
use App\Services\EventFetcher;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Services\ArticleFetcher;


class MediaController extends Controller
{

    public function __construct(Article $article,Event $event,ArticleFetcher $articleFetcher,EventFetcher $eventFetcher)
    {
        $this->articles = $article;
        $this->event = $event;
        $this->articleFetcher = $articleFetcher;
        $this->eventFetcher = $eventFetcher;
    }

    public function news(){
        $articles = $this->articleFetcher->getByCategory(5,6);

        return view('pages.media.news',compact('articles'));
    }

    public function newsletters(){
        $articles = $this->articleFetcher->getByCategory(9,6);

        return view('pages.patient-newsletter.index',compact('articles'));
    }

    public function campaigns(){
        $articles = $this->articleFetcher->getByCategory(6,6);

        return view('pages.media.hospital-campaigns',compact('articles'));
    }

    public function seminars(){
        $articles = $this->eventFetcher->getByCategory('seminar',6);

        return view('pages.media.seminars-and-workshops',compact('articles'));
    }

    public function single($slug){
        $article = $this->articles->where('slug',$slug)->first();

        if(!$article)
            return redirect()->route('media.news');

        $article->views++;
        $article->save();

        return view('pages.media.single',compact('article'));
    }

    public function singleNewsletter($slug){
        $article = $this->articles->where('slug',$slug)->first();

        if(!$article)
            return redirect()->route('media.news');

        $article->views++;
        $article->save();

        return view('pages.patient-newsletter.single',compact('article'));
    }

    public function singlePage($slug){
        $article = $this->articles->where('slug',$slug)->first();

        if(!$article)
            return redirect()->route('media.news');

        $article->views++;
        $article->save();

        return view('pages.page',compact('article'));
    }

    public function singleEvent($slug){
        $article = $this->event->where('slug',$slug)->first();

        if(!$article)
            return redirect()->route('media.news');

        return view('pages.media.single-event',compact('article'));
    }

    public function galleries(){
        $galleries = Album::orderBy('created_at','DESC')->paginate(12);

        if(!$galleries)
            return redirect()->route('media.news');

        return view('pages.media.galleries',compact('galleries'));
    }

    public function album($slug){
        $data = Album::with('contents.uploads')->where('slug',$slug)->first();

        if(!$data)
            return redirect()->route('media.news');

        return view('pages.media.album',compact('data'));
    }
}
