<?php

namespace App\Http\Controllers\Admin;

use App\Models\Album;
use App\Models\AlbumContent;
use App\Repositories\GalleryManager;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;

class GalleryController extends Controller
{

    public function __construct(Album $model, AlbumContent $content, GalleryManager $manager)
    {
        $this->model = $model;
        $this->content = $content;
        $this->galleries = $manager;
    }

    public function filter($cat){
        $data = Album::where('type',$cat)->orderBy('created_at','DESC')->get();
        
        return view('admin.galleries.index',compact('data'));
    }

    public function create(){
        return view('admin.galleries.create');
    }

    public function edit($id){
        $data = $this->model->with('contents.uploads')->where('id',$id)->first();

        return view('admin.galleries.create',compact('data'));
    }

    public function store(Request $request){
        $data = $this->galleries->store($request);

        if($data){
            Session::flash('success','Saved Successfully');
            return redirect(route('admin.galleries.edit',$data->id));
        }

        Session::flash('error','There was an error. Please try again.');
        return redirect()->back();
    }

    public function update(Request $request){
        $this->galleries->update($request);

        Session::flash('success','Updated Successfully');
        return redirect()->back();
    }

    public function delete($id){
        $this->model->where('id',$id)->delete();

        Session::flash('success','Deleted Successfully');
        return redirect()->back();
    }

    public function deleteItem($id){
        $this->content->where('id',$id)->delete();

        Session::flash('success','Deleted Successfully');
        return redirect()->back();
    }
}
