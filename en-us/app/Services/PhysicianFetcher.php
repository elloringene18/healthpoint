<?php
namespace App\Services;

use App\Models\Option;
use App\Models\Physician;
use App\Repositories\Traits\Fetcher;

class PhysicianFetcher {


    public function __construct(Physician $model)
    {
        $this->model = $model;
    }


    public function getAll(){
        return $this->model->orderBy('lname','ASC')->get();
    }

    public function getLimit($limit){
        return $this->model->orderBy('created_at','desc')->limit($limit)->get();
    }

    public function filter(){
        $query = Physician::query();

        if(isset($_GET['gender']))
            if($_GET['gender']!='any')
                $query = $query->where('gender',$_GET['gender']);

        if(isset($_GET['specialty']))
            if($_GET['specialty']!='any'){
                $query = $query->whereHas('department', function($q){
                    $q->where('slug',$_GET['specialty']);
                });
            }
        if(isset($_GET['name']))
            if($_GET['name'])
                $query = $query->where('fname','like','%'.$_GET['name'].'%')->orWhere('lname','like','%'.$_GET['name'].'%');

        return $query;
    }

    public function getFeatured(){
        $option = Option::where('slug','featured-physician')->first();
        if($option){

            $physician = $this->model->where('id',$option->value)->first();

            if($physician)
                return $physician;
        }

        return $this->model->first();
    }

    public function getList(){
        $physicians = $this->model->orderBy('lname','ASC')->get();

        foreach($physicians as $physician)
            $data[$physician->id] = $physician->name;

        return $data;
    }

}