<?php
namespace App\Services;

use App\Models\Article;
use App\Models\Option;
use App\Repositories\Traits\Fetcher;

class OptionFetcher {

    public function __construct(Option $model)
    {
        $this->model = $model;
    }

    public function getBySlug($slug)
    {
        $data = $this->model->where('slug',$slug)->first();

        if($data)
            return $data->value;

        return $data;
    }

    public function getAll(){
        $options = $this->model->where('slug','!=','survey-emails')->get();

        $data = [];

        foreach ($options as $key=>$option){
            $data[$option->slug] = $option;
        }

        $data['survey-emails'] = $this->model->where('slug','survey-emails')->pluck('value');

        return $data;
    }

    public function getLatestNews()
    {
        return $this->model->where('category_id',5)->orderBy('created_at','DESC')->first();
    }
}